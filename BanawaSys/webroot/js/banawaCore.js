function closeOverlay() {
    $(".banawa-overlay.banawa-quantity-container").css("display","none");
    makeBackgroundOrange($(".banawa-quantity-input"));
    makeBackgroundOrange($(".banawa-option-input"));
}

function showOverlayAlert() {
     $.alert({
            title: '¡Prohibido!',
            content: 'Debe seleccionar <strong>UNA OPCION</strong> e ingresar <strong>UNA CANTIDAD MAYOR A 0</strong> antes de continuar',
            type: 'red',
    });
}

function deleteLastInputVal() {
    var oldValue = $(".banawa-quantity-input").children("input").val().toString();
    var newValue = "";
    if (oldValue.length < 1) {
        newValue = "";
        makeBackgroundOrange($(".banawa-quantity-input"));
    } else {
        newValue = oldValue.substring(0, oldValue.length - 1); 
    }
    $(".banawa-quantity-input").children("input").val(parseInt(newValue));
}

function clusterSameProducts(prdArray) {
    var cluster = {};
    for (var i = 0; i < prdArray.length; i++) {
        if (cluster[prdArray[i].thumbnail] == null) {
            cluster[prdArray[i].thumbnail] = prdArray[i];
            cluster[prdArray[i].thumbnail]["prices"] = [];
            cluster[prdArray[i].thumbnail]["options"] = [];
            cluster[prdArray[i].thumbnail]["ids"] = [];
        }
        var thumbnail = prdArray[i].thumbnail.split("/");
        cluster[prdArray[i].thumbnail]["prices"].push(prdArray[i].price);
        thumbnail = thumbnail[thumbnail.length - 1].split(".")[0].replace("-", " ").toUpperCase();

        var newOption = prdArray[i].name.toUpperCase().split(thumbnail);
        cluster[prdArray[i].thumbnail]["options"].push(newOption[newOption.length-1]);

        cluster[prdArray[i].thumbnail]["ids"].push(prdArray[i].id);        
        if (cluster[prdArray[i].thumbnail]["prices"].length > 1) {
            cluster[prdArray[i].thumbnail]["name"] = thumbnail;
        }
    }
    return cluster;
}

var step = 0;
var actualCategory = -1;
var saleProducts = [];
var sale = {};

function setSaleType() {
    console.log($(this));
    if (type=="boleta") {
        sale.type = "boleta";
    } else if (type == "factura") {
        renderInputFactura();
    } else if (type == "facturaCompleta") {
        sale.type = "factura";
        sale.razon_social = $("input[name='razon-social']").val();
        sale.ruc = $("input[name='RUC']").val();
    }
}

function renderInputFactura() {
    $(".banawa-boleta").hide();
    $(".banawa-input-razon-social").show();
    $(".banawa-input-ruc").show();
    $(".banawa-factura").attr("data-type", "facturaCompleta");
}

function renderSaleTypes() {
    $.confirm({
        title: 'Generar Pedido',
        content: ''+
        '<form action="" class="formName">' +
        '<div class="form-group">' +
        '<label>¿A QUE NOMBRE PONGO EL PEDIDO?<span style="color: red;">*</span></label>' +
        '<input type="text" name="order-name" placeholder="¡Bienvenido Sr(a): " class="name form-control" required />' +
        '</div>' +
        '<div class="form-group">' +
        '<label>¿CON CUANTO VA A PAGAR?<span style="color: red;">*</span></label>' +
        '<input type="number" name="pay-amount" placeholder="Monto de Pago" class="name form-control" required />' +        
        '</div>' +
        '</form>'+
        '<label>¿Boleta o Factura?</label>',
        closeIcon: true,
        type: 'yellow',
        buttons: {
            notaVenta: {
                text: 'N. V.',
                btnClass: 'btn-blue',
                keys: ['enter'],
                action: function() {
                    console.log("vals: " + $("input[name=order-name]").val());
                    console.log("vals: " + $("input[name=pay-amount]").val());
                    if($("input[name=order-name]").val().length == 0 || $("input[name=pay-amount]").val().length == 0) {
                        $.alert({
                                title: '¡Prohibido!',
                                content: 'Debe ingresar un valor en todos los campos requeridos',
                                type: 'red',
                        });
                    } else {
                        sale.type = "nota_venta";
                        sale.total_price = getTotalPrice();
                        sale.order_name = $("input[name=order-name]").val();
                        sale.pay_amount = $("input[name=pay-amount]").val();
                        sale.pay_change = Number( parseFloat(sale.pay_amount)- parseFloat(sale.total_price)).toFixed(2);
                        sale.caja_id = $(".banawa-container").attr("data");
                        sale.productos = getProductsToSave(sale.productos);
                        addDateToObj(sale);
                        sendSaleData();
                    }
                }
            },
            boleta: {
                text: 'BOLETA',
                btnClass: 'btn-blue',
                keys: ['enter'],
                action: function() {
                    console.log("vals: " + $("input[name=order-name]").val());
                    console.log("vals: " + $("input[name=pay-amount]").val());
                    if($("input[name=order-name]").val().length == 0 || $("input[name=pay-amount]").val().length == 0) {
                        $.alert({
                                title: '¡Prohibido!',
                                content: 'Debe ingresar un valor en todos los campos requeridos',
                                type: 'red',
                        });
                    } else {
                        sale.type = "boleta";
                        sale.total_price = getTotalPrice();
                        sale.order_name = $("input[name=order-name]").val();
                        sale.pay_amount = $("input[name=pay-amount]").val();
                        sale.pay_change = Number( parseFloat(sale.pay_amount)- parseFloat(sale.total_price)).toFixed(2);
                        sale.caja_id = $(".banawa-container").attr("data");
                        sale.productos = getProductsToSave(sale.productos);
                        addDateToObj(sale);
                        sendSaleData();
                    }
                }
            },
            factura: {
                text: 'FACTURA',
                btnClass: 'btn-blue',
                keys: ['enter'],
                action: function () {
                    if($("input[name=order-name]").val().length == 0 || $("input[name=pay-amount]").val().length == 0) {
                        $.alert({
                                title: '¡Prohibido!',
                                content: 'Debe ingresar un valor en todos los campos requeridos',
                                type: 'red',
                        });
                    } else {
                        sale.order_name = $("input[name=order-name]").val();
                        sale.pay_amount = $("input[name=pay-amount]").val();
                        $.confirm({
                            title: 'Ingrese datos requeridos',
                            content: ''+
                            '<form action="" class="formName">' +
                            '<div class="form-group">' +
                            '<label>RAZON SOCIAL<span style="color: red;">*</span></label>' +
                            '<input type="text" name="razon_social" placeholder="Razon Social" class="name form-control" required />' +
                            '</div>' +
                            '<div class="form-group">' +
                            '<label>RUC<span style="color: red;">*</span></label>' +
                            '<input type="number" name="ruc" placeholder="Ruc" class="name form-control" pattern="[0-9]{11}" required />' +
                            '</div>' +
                            '</form>',
                            closeIcon: true,
                            type: 'yellow',
                            buttons: {
                                facturar: {
                                    text: 'FACTURA',
                                    btnClass: 'btn-blue',
                                    keys: ['enter'],
                                    action: function () {
                                        if($("input[name=ruc]").val().length == 0 || $("input[name=razon_social]").val().length == 0) {
                                            $.alert({
                                                    title: '¡Prohibido!',
                                                    content: 'Debe ingresar un valor en todos los campos requeridos',
                                                    type: 'red',
                                            });
                                        } else {
                                            sale.type = "factura";
                                            sale.factura_RUC = $("input[name=ruc]").val();
                                            sale.factura_razon_social = $("input[name=razon_social]").val();
                                            sale.total_price = getTotalPrice();
                                            sale.pay_change = Number( parseFloat(sale.pay_amount)- parseFloat(sale.total_price)).toFixed(2);
                                            sale.caja_id = $(".banawa-container").attr("data");
                                            sale.productos = getProductsToSave(sale.productos);
                                            addDateToObj(sale);
                                            sendSaleData();
                                        }
                                    }
                                },
                                cancel: function () {
                                },
                            }
                        });
                    }
                }
            },
            cancel: function() {
                //btnClass: 'btn-red',
            }
        }
    });
    //$(".banawa-sale-type-container").show();
}

function getProductsToSave() {
     var productsToSave = {};
    for (var i = 0; i < saleProducts.length; i++) {
        productsToSave[saleProducts[i].id] = saleProducts[i].amount;
    }
    return productsToSave;
}

function getTotalPrice() {
    var totalPrice = 0;
    for (var i = 0; i < saleProducts.length; i++) {
        totalPrice += saleProducts[i].amount * saleProducts[i].price;
    }
    return totalPrice;
}

function sendSaleData() {
    $.dialog({
        title: 'Procesando...',
        closeIcon: true,
        type: 'orange',
        content: 'Espere un momento, por favor',
        icon: 'fa fa-spinner fa-spin'
    });

    var formatedData = sale;
    //console.log(formatedData);
    $.ajax({
      url: window.location.origin+"/ventaapi/add.json",
      method: "POST",
      dataType: "json",
      data: formatedData,
      success: function(rpta) {
        $(".jconfirm-closeIcon").trigger("click");
        
        $.dialog({
            title: '¡Exito!',
            content: '¡Venta Realizada!',
            closeIcon: true,
            type: 'green',
        });

        //Clean everything to start another sale
        $(".banawa-order-list").html("");
        saleProducts = [];
        sale = {};
        renderPriceAll();
      }
    })
    .fail(function(result) {
        window.result = result.responseText;
        $.dialog({
            title: 'Error',
            content: 'No se pudo procesar la venta: ' + result,
            closeIcon: true,
            type: 'red',
        });
    });
}

function modifyProductListQuantity() {
    modifyProductQuantityById($(".banawa-quantity-input").children("input").attr("data"), $(".banawa-quantity-input").children("input").val());
    updateProductPriceById($(".banawa-quantity-input").children("input").attr("data"));
    closeOverlay();
    renderPriceAll();
    cleanQuantityInputVal();
    changeContinueFunction("modifyProductQuantity();");
}


function modifyProductQuantity() {
    if (alreadyInList($(".banawa-option-input").children(".banawa-option").children("input:checked")) && ($(".banawa-option-input").parent().css("display") != "none")) {
        addProductQuantityById($(".banawa-option-input").children(".banawa-option").children("input:checked"), $(".banawa-quantity-input").children("input").val());
        updateProductPriceById($(".banawa-option-input").children(".banawa-option").children("input:checked").attr("data-id"));
        removeAlreadyInListItem($(".banawa-order-list"));
        closeOverlay();
        cleanOptionsInput();
        cleanQuantityInputVal();
    } else {
        if (($(".banawa-option-input").children(".banawa-option").children("input:checked").length == 0 && 
            ($(".banawa-option-input").parent().css("display") != "none")) || $(".banawa-quantity-input").children("input").val().length == 0) {
            showOverlayAlert();
        } else{
            if(($(".banawa-option-input").parent().css("display") != "none")) {
                modifyProductIdByOption($(".banawa-option-input").children(".banawa-option").children("input:checked"), $(".banawa-quantity-input").children("input"));
            }
            modifyProductQuantityById($(".banawa-quantity-input").children("input").attr("data"), $(".banawa-quantity-input").children("input").val());
            modifyProductPriceById($(".banawa-quantity-input").children("input").attr("data"), $(".banawa-quantity-input").children("input").val());
            if ($(".banawa-option-input").children(".banawa-option").children("input:checked").length == 0 && ($(".banawa-option-input").parent().css("display") != "none")) {
                showOverlayAlert();
            } else {
                modifyProductDataByOption($(".banawa-option-input").children(".banawa-option").children("input:checked"), $(".banawa-quantity-input").children("input").val());
                $(".banawa-quantity-input").children("input").val("");
                closeOverlay();
                cleanOptionsInput();
                cleanQuantityInputVal();
            }
        }
    }
    renderPriceAll();
}

function cleanQuantityInputVal() {
    $(".banawa-quantity-input").children('input').val('');
}

function removeAlreadyInListItem(list) {
    console.log(jQuery(list).children(":last"));
    if( jQuery(list).children(":last").attr("data").split(",").length > 1 )
    {
        saleProducts.splice(saleProducts.length - 1,1);
        jQuery(list).children(":last").remove();
    }
}

function addProductQuantityById(optionId, val) {
    var amountCarry = 0;

    for (var i = 0; i < saleProducts.length; i++) {
        if (saleProducts[i].id == optionId.attr("data-id")) {
            saleProducts[i].amount = Number(saleProducts[i].amount) + Number(val);
            amountCarry = saleProducts[i].amount;
        }
    }

    $(".banawa-order-item").each(function(index) {
        if ($(this).attr("data") == optionId.attr("data-id")) {
            var oldText = $(this).children(".banawa-list-buttons").children(".banawa-quantity-button").html();
            var newText = amountCarry;
            $(this).children(".banawa-list-buttons").children(".banawa-quantity-button").html('');
            $(this).children(".banawa-list-buttons").children(".banawa-quantity-button").html(amountCarry);
        }
    });
}

function updateProductPriceById(optionId) {
    var actualAmount = 0;
    var actualPrice = 0;

    for (var i = 0; i < saleProducts.length; i++) {
        if (saleProducts[i].id == optionId) {
            actualAmount = saleProducts[i].amount;
            actualPrice = saleProducts[i].price;
        }
    }

    $(".banawa-order-item").each(function(index) {
        if ($(this).attr("data") == optionId) {
            $(this).children(".banawa-list-buttons").children(".banawa-price-button").html('');
            $(this).children(".banawa-list-buttons").children(".banawa-price-button").html("S/. " + (Number(actualPrice*actualAmount).toFixed(2)).toString());
        }
    });
}

function alreadyInList(checkedInput) {
    if(checkedInput.length == 0) {
        return false;
    }

    for (var i = 0; i < saleProducts.length; i++) {
        if (saleProducts[i].id == checkedInput.attr("data-id")) {
            return true;
        }
    }
    return false;
}

function modifyProductIdByOption(checkedInput, quantityInput) {
    if(checkedInput.length > 0) {
        quantityInput.attr("data", checkedInput.attr("data-id"));
    }
}

function modifyProductDataByOption(option, val) {
    if (val.length == 0) {
        val = 0;
        return;
    }

    var optionId = jQuery(option).attr("data-id");
    var saleProductId = "";
    var originalPrice = 0;
    $(".banawa-order-item").each(function(index) {
        if ($(this).attr("data").split(",").indexOf(optionId) != -1) {
            saleProductId = $(this).attr("data");
            var objPos = $(this).attr("data").split(",").indexOf(optionId);
            console.log("posicion: "+objPos);
            $(this).attr("data", optionId);
            $(this).children(".banawa-list-buttons").children(".banawa-price-button").html('');
            var newText = $(this).children(".banawa-list-buttons").parent().children("a").children(".banawa-title").text()+jQuery(option).val();
            $(this).children(".banawa-list-buttons").parent().children("a").children(".banawa-title").text(newText);
            originalPrice = $(this).children(".banawa-list-buttons").children(".banawa-price-button").attr('data-price').split(",")[objPos];
            console.log($(this).children(".banawa-list-buttons").children(".banawa-price-button").attr('data-price').split(","));
            $(this).children(".banawa-list-buttons").children(".banawa-price-button").html("S/. " + (Number(originalPrice*val).toFixed(2)).toString());
        }
    });

    for (var i = 0; i < saleProducts.length; i++) {
        if (saleProducts[i].id == saleProductId) {
            saleProducts[i].id = optionId;
            saleProducts[i].price = originalPrice;
            saleProducts[i].name += jQuery(option).val();
            break;
        }
    }
}

function modifyProductPriceById(prd_id, val) {
    if (val.length == 0) {
        val = 0;
    }
    $(".banawa-order-item").each(function(index) {
        if ($(this).attr("data").split(",").indexOf(prd_id) != -1) {
            $(this).children(".banawa-list-buttons").children(".banawa-price-button").html('');
            var originalPrice = $(this).children(".banawa-list-buttons").children(".banawa-price-button").attr('data-price');
            $(this).children(".banawa-list-buttons").children(".banawa-price-button").html("S/. " + (Number(originalPrice*val).toFixed(2)).toString());
        }
    });
}

function modifyProductQuantityById(prd_id, val) {
    if (val.length == 0) {
        val = 0;
    }

    for (var i = 0; i < saleProducts.length; i++) {
        if (saleProducts[i].id.split(",").indexOf(prd_id) != -1) {
            saleProducts[i].amount = val;
            break;
        }
    }

    $(".banawa-order-item").each(function(index) {
        if ($(this).attr("data").split(",").indexOf(prd_id) != -1) {
            var oldText = $(this).children(".banawa-list-buttons").children(".banawa-quantity-button").html();
            var newText = val;
            $(this).children(".banawa-list-buttons").children(".banawa-quantity-button").html('');
            $(this).children(".banawa-list-buttons").children(".banawa-quantity-button").html(val);
        }
    });

}

function renderSaleOrder() {
    if ($(".banawa-order-list").length == 0) {
        $(".banawa-order").prepend('<div class="banawa-order-list"></div>');
    } else {
        $(".banawa-order-list").html('');
    }

    for (var i = 0; i < saleProducts.length; i++){
        var saleItem = saleProducts[i];
        var saleItemAmount = 0;
        if (saleItem["amount"] != null) {
            saleItemAmount = saleItem["amount"];
        }
        $(".banawa-order-list").append('<div class="banawa-order-item banawa-index-card banawa-background-img" data="'+saleItem["id"]
            +'" data-price="'+saleItem["price"]+'""><a> <img src="'+saleItem["thumbnail"]+'" />'+
            '<div class="banawa-title">'+saleItem["name"]+'</div></a>'+
            '<div class="banawa-list-buttons"><div class="banawa-cancel-button"><img src="images/close-button.png" /></div>'+
            '<div class="banawa-quantity-button">'+saleItemAmount+'</div>'+
            '<div class="banawa-price-button" data-price="'+saleItem["price"]+'">S/. '+Number(parseFloat(saleItem["price"])*saleItemAmount).toFixed(2)+'</div></div>');
    }
    $(".banawa-cancel-button").on("click", function(){
        if(confirm("¿Esta seguro que desea elminar el elemento de la venta?")) {
            removeSaleProduct($(this).parent().parent());
        }
    });
    $(".banawa-quantity-button").on("click", function(){
        renderOptionsOverlay($(this).parent().parent().attr("data"),$(this).text());
        changeContinueFunction("modifyProductListQuantity();");
    });
    renderPriceAll();
}

function changeContinueFunction(newFunction) {
    $(".banawa-continue").attr("onclick", newFunction);
}

function makeBackgroundGreen(element) {
    jQuery(element).css("background-color", "#57c30b");
}

function makeBackgroundOrange(element) {
    jQuery(element).css("background-color", "#e9882a");
}

function cleanOptionsInput() {
    $(".banawa-option-input").html('');
    $(".banawa-option-input").parent().hide();
}

function cleanCheckedFormOtherOptions() {
    $(".banawa-checkbox-option").prop('checked', false);
}

function renderPriceAll() {
    var saleTotalPrice = 0;
    for (var i = 0; i < saleProducts.length; i++){
        var saleItem = saleProducts[i];
        saleTotalPrice +=  parseFloat(saleItem["price"])*parseInt(saleItem["amount"]);
    }
    $(".banawa-price-all-button").html('S/. ' + Number(saleTotalPrice).toFixed(2));    
}

function renderOptionsOverlay(prd_id, prd_quantity=0) {
    $(".banawa-overlay.banawa-quantity-container").css("display","inline-block");

    $(".banawa-quantity-input").children('input').attr("data", prd_id);

    // TODO: Fix prefill. Not done because it generates confusion to the user 
    // if (prd_quantity != 0) {
    //     $(".banawa-quantity-input").children('input').val(prd_quantity);
    // }
}

function rederOptionsData(clusteredPrdObj) {
    var clusterPrices = clusteredPrdObj.attr("data-price").split(",");
    var clusterIds = clusteredPrdObj.attr("data-ids").split(",");
    var clusterOptions = clusteredPrdObj.attr("data-options").split(",");
    if (clusterPrices.length == 1) {
        $(".banawa-option-input").html('');
        $(".banawa-option-input").parent().hide();
    } else {
        //var name = jQuery(clusteredPrdObj).children().children(".banawa-title").text();
        $(".banawa-option-input").html('');
        $(".banawa-option-input").append('<div class="col-md-12 col-12 banawa-info">ESCOJA UNA OPCION:</div><div class="clearfix"><br/></div>');
        for (var i = 0;i < clusterPrices.length; i++) {
            $(".banawa-option-input").append('<div class="col-md-12 col-12 banawa-option"><input class="banawa-checkbox-option" type="checkbox" name="'+clusterOptions[i]
                +'" value="'+clusterOptions[i]+'" data-id="'+clusterIds[i]+'">'+
                clusterOptions[i]+'<span class="banawa-option-price">S/.'+clusterPrices[i]+'</span><br></div>');
        }
        makeBackgroundOrange($(".banawa-option-input"));
        $(".banawa-checkbox-option").on("click", function() {
            cleanCheckedFormOtherOptions();
            $(this).prop('checked', true);
            makeBackgroundGreen($(".banawa-option-input"));
        });
        $(".banawa-option-input").parent().show();
    }
}

function renderCategories(visible=true) {
    if (visible) {
        $(".banawa-categorias").css("display","");
        $(".banawa-backwards").css("display","none");
        $(".banawa-productos").html('');
    } else {
        $(".banawa-categorias").css("display","none");
        $(".banawa-backwards").css("display","inline-block");
    }
}


function getProductsByCat(cat_id) {
    $.ajax({
      url: window.location.origin+"/productoapi/get_prd_by_cat/"+ cat_id +".json",
      method: "GET",
      dataType: "json",
      success: function(rpta) {
        if ($(".banawa-productos").length == 0) {
            $(".banawa-home").append('<div class="banawa-productos row"></div>');
        } else {
            $(".banawa-productos").html('');
        }

        renderCategories(false);
        var clusteredProducts = clusterSameProducts(rpta.productos);
        for  (var key in clusteredProducts){
            var prd = clusteredProducts[key];
            $(".banawa-productos").append(
                '<div class="banawa-producto banawa-button banawa-background-img col-md-3 col-sm-4 col-4" data="'+prd["id"]
                +'" data-price="'+prd["prices"]+'" data-ids="'+prd["ids"]+'" data-options="'+prd["options"]+'"><a> <img src="'+key+'"/>'+
                '<div class="banawa-title">'+prd["name"]+'</div></a></div>');
        }

        $(".banawa-producto").on("click", function(){
            appendSaleProduct($(this));
            rederOptionsData($(this));
            renderSaleOrder();
            var saleItemAmount = 0;
            for (var i = 0; i < saleProducts.length; i++){
                var saleItem = saleProducts[i];
                if (saleItem["amount"] != null && saleItem["id"] == $(this).attr("data")) {
                    saleItemAmount = saleItem["amount"];
                    break;
                }
            }
            renderOptionsOverlay($(this).attr("data"), saleItemAmount);
        });
      }
    });
}

function appendSaleProduct(prd_obj) {
    isRepeated = false;
    for (var i = 0; i < saleProducts.length; i++) {
        if (saleProducts[i].id == prd_obj.attr("data-ids")) {
            isRepeated = true;
            break;
        }
    }

    var newProduct = {
        "id": prd_obj.attr("data-ids"),
        "price": prd_obj.attr("data-price"),
        "thumbnail": prd_obj.children().children("img").attr("src"),
        "name": prd_obj.children().children(".banawa-title").text()
    };
    
    if (!isRepeated) {
        saleProducts.push(newProduct);
    }
}

function removeSaleProduct(prd_obj) {
    var removeIndex = -1;
    for (var i = 0; i < saleProducts.length; i++) {
        if (prd_obj.attr("data")== saleProducts[i].id) {
            removeIndex = removeIndex + i + 1;
        }
    }
    console.log(removeIndex);

    if (removeIndex != -1) {
        saleProducts.splice(removeIndex, 1);
    }
    renderSaleOrder();
}

function addDateToObj(obj) {
    var date = new Date();

    obj.date = {};
    obj.date["day"] = date.getDate();
    obj.date["month"] = date.getMonth() + 1;
    obj.date["year"] = date.getFullYear();
    obj.date["hour"] = date.getHours();
    obj.date["minute"] = date.getMinutes();

}

$(document).ready(function(){
    $(".categoria").on("click", function(){
        getProductsByCat($(this).attr("data"));
    });

    $(".banawa-quantity-number").on("click", function(){
        var newVal = $(".banawa-quantity-input").children("input").val().toString() + $(this).text();
        $(".banawa-quantity-input").children("input").val(parseInt(newVal));
        makeBackgroundGreen($(".banawa-quantity-input"));
    });

    $("input[name='item-quantity']").on("input", function(){
        if ($(this).val().length == 0) {
            makeBackgroundOrange($(this).parent());
        } else {
            makeBackgroundGreen($(this).parent());
        }
    });

    window.saleProducts = saleProducts;
    window.sale = sale;
});
