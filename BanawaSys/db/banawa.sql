-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2018 at 12:32 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banawa`
--

-- --------------------------------------------------------

--
-- Table structure for table `caja`
--

CREATE TABLE `caja` (
  `id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `type` varchar(15) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `original_password` varchar(50) NOT NULL,
  `location` varchar(150) NOT NULL,
  `workers_amount` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `caja`
--

INSERT INTO `caja` (`id`, `name`, `type`, `user`, `password`, `original_password`, `location`, `workers_amount`) VALUES
(1, 'Andres Vargas', 'admin', 'admin', '$2y$10$jlRgjsfmLOiJfA4ZBLO2E.P6NfP0kuhC29YosQEFF6X', 'adminawa_0702$$', 'Local del Centro. Primer Local', 0),
(2, 'Caja01', 'cashier', 'caja1', '$2y$10$3/JtDTsKJfMSoAfQB8wQ5OCh6jv0u5tdYPGn8k9f2AB', 'banawa_01', 'Local del Centro. Primer Local', 1),
(3, 'Caja02', 'cashier', 'caja2', '$2y$10$6F3/EC6vVXNizPJEYDUQb.eP/.diasTOmhwntGgaMb8', 'caja02', 'Local del Centro. Primer Local', 1);

-- --------------------------------------------------------

--
-- Table structure for table `caja_empleado`
--

CREATE TABLE `caja_empleado` (
  `id` int(11) NOT NULL,
  `hire_date` date NOT NULL,
  `empleado_id` int(11) NOT NULL,
  `caja_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `thumbnail` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id`, `name`, `thumbnail`) VALUES
(1, 'Jugos Combinados', 'images/categories/jugos_combinados.jpg'),
(2, 'Jugos Puros', 'images/categories/jugos_puros.jpg'),
(3, 'Cocktails Banawa', 'images/categories/cocktails_banawa.jpg'),
(4, 'Smoothies Banawa', 'images/categories/smoothies_banawa.jpg'),
(5, 'Sandwichs', 'images/categories/sandwichs.jpg'),
(6, 'Ensaladas de Frutas', 'images/categories/ensaladas_frutas.jpg'),
(7, 'Yogurt Jar', 'images/categories/yogurt_yard.jpg'),
(8, 'Cafe', 'images/categories/cafe.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `descuentos`
--

CREATE TABLE `descuentos` (
  `id` int(11) NOT NULL,
  `type` varchar(15) NOT NULL,
  `price` float NOT NULL,
  `quantity` int(11) NOT NULL,
  `avalible_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `empleado`
--

CREATE TABLE `empleado` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `failures` mediumint(9) DEFAULT NULL,
  `payment` float NOT NULL,
  `hire_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `insumo`
--

CREATE TABLE `insumo` (
  `id` int(11) NOT NULL,
  `quantity` float NOT NULL,
  `measure_unity` varchar(50) NOT NULL,
  `buy_amount` float NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `buy_date` date NOT NULL,
  `provider` varchar(150) NOT NULL,
  `waste` float NOT NULL,
  `avalible_items` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `thumbnail` varchar(150) DEFAULT NULL,
  `descuento_id` int(11) DEFAULT NULL,
  `categoria_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`id`, `name`, `price`, `thumbnail`, `descuento_id`, `categoria_id`, `active`) VALUES
(1, 'La Fresa 12oz.', 5.9, 'images/products/la-fresa.jpg', NULL, 1, 1),
(2, 'La Fresa 16oz.', 8.9, 'images/products/la-fresa.jpg', NULL, 1, 1),
(3, 'El Guapo 12oz', 5.9, 'images/products/el-guapo.jpg', NULL, 1, 1),
(4, 'El Guapo 16oz', 8.9, 'images/products/el-guapo.jpg', NULL, 1, 1),
(5, 'Maracuyeah 12oz', 5.9, 'images/products/maracuyeah.jpg', NULL, 1, 1),
(6, 'Maracuyeah 16oz', 8.9, 'images/products/maracuyeah.jpg', NULL, 1, 1),
(7, 'Mapache 12oz', 5.9, 'images/products/mapache.jpg', NULL, 1, 1),
(8, 'Mapache 16oz', 8.9, 'images/products/mapache.jpg', NULL, 1, 1),
(9, 'Guasón 12oz', 5.9, 'images/products/guason.jpg', NULL, 1, 1),
(10, 'Guasón 16oz', 8.9, 'images/products/guason.jpg', NULL, 1, 1),
(11, 'Ponchawa 12oz', 5.9, 'images/products/ponchawa.jpg', NULL, 1, 1),
(12, 'Ponchawa 16oz', 8.9, 'images/products/ponchawa.jpg', NULL, 1, 1),
(13, 'Fresa 12oz', 5.9, 'images/products/fresa.jpg', NULL, 2, 1),
(14, 'Fresa 16oz', 7.9, 'images/products/fresa.jpg', NULL, 2, 1),
(15, 'Mango 12oz', 5.9, 'images/products/mango.jpg', NULL, 2, 1),
(16, 'Mango 16oz', 7.9, 'images/products/mango.jpg', NULL, 2, 1),
(17, 'Piña 12oz', 5.9, 'images/products/pina.jpg', NULL, 2, 1),
(18, 'Piña 16oz', 7.9, 'images/products/pina.jpg', NULL, 2, 1),
(19, 'Papaya 12oz', 5.9, 'images/products/papaya.jpg', NULL, 2, 1),
(20, 'Papaya 16oz', 7.9, 'images/products/papaya.jpg', NULL, 2, 1),
(21, 'Platano 12oz', 5.9, 'images/products/platano.jpg', NULL, 2, 1),
(22, 'Platano 16oz', 7.9, 'images/products/platano.jpg', NULL, 2, 1),
(23, 'Fresa con Leche 12oz', 6.9, 'images/products/fresa.jpg', NULL, 2, 1),
(24, 'Fresa con Leche 16oz', 8.9, 'images/products/fresa.jpg', NULL, 2, 1),
(25, 'Mango con Leche 12oz', 6.9, 'images/products/mango.jpg', NULL, 2, 1),
(26, 'Mango con Leche 16oz', 8.9, 'images/products/mango.jpg', NULL, 2, 1),
(27, 'Piña con Leche 12oz', 6.9, 'images/products/pina.jpg', NULL, 2, 1),
(28, 'Piña con Leche 16oz', 8.9, 'images/products/pina.jpg', NULL, 2, 1),
(29, 'Papaya con Leche 12oz', 6.9, 'images/products/papaya.jpg', NULL, 2, 1),
(30, 'Papaya con Leche 16oz', 8.9, 'images/products/papaya.jpg', NULL, 2, 1),
(31, 'Platano con Leche 12oz', 6.9, 'images/products/platano.jpg', NULL, 2, 1),
(32, 'Platano con Leche 16oz', 8.9, 'images/products/platano.jpg', NULL, 2, 1),
(33, 'Cafe Pasado', 4.5, 'images/products/cafe-pasado.jpg', NULL, 8, 1),
(34, 'Blueberry, Fresa, Zumo de Limón y Pisco peruano', 15, 'images/products/blueberry-fresa-limon-goma-pisco.jpg', NULL, 3, 1),
(35, 'Piña, Mango, Crema de Coco y Pisco peruano', 15, 'images/products/pina-coco-crema-coco-pisco.jpg', NULL, 3, 1),
(36, 'Lúcuma, Leche, Canela y Pisco peruano', 15, 'images/products/lucuma-leche-canela-pisco.jpg', NULL, 3, 0),
(37, 'Tuna, Menta, Jarabe de goma y Pisco peruano', 15, 'images/products/tuna-menta-goma-pisco.jpg', NULL, 3, 0),
(38, 'Pisco Sour', 15, 'images/products/pisco-sour.jpg', NULL, 3, 1),
(39, 'Mojito', 15, 'images/products/mojito.jpg', NULL, 3, 1),
(40, 'Pituberry', 15, 'images/products/pituberry.jpg', NULL, 4, 1),
(41, 'Gitano', 15, 'images/products/gitano.jpg', NULL, 4, 1),
(42, 'Franfresa', 15, 'images/products/framfresa.jpg', NULL, 4, 1),
(43, 'Cocobongo', 15, 'images/products/cocobongo.jpg', NULL, 4, 1),
(44, 'Popeye', 15, 'images/products/popeye.jpg', NULL, 4, 1),
(45, 'Sojoro', 15, 'images/products/sojoro.jpg', NULL, 4, 1),
(46, 'Ensaladas de Frutas Banawa', 9.9, 'images/products/ensaladas-frutas-banawa.jpg', NULL, 6, 1),
(47, 'Yogurt Jar', 9.9, 'images/products/yogurt-yard.jpg', NULL, 7, 1),
(48, 'Grilled cheese sandwich', 5, 'images/products/grilled-cheese-sandwich.jpg', NULL, 5, 1),
(49, 'Capresse', 8, 'images/products/capresse.jpg', NULL, 5, 1),
(50, 'Sandwich de Jamón y Queso', 7, 'images/products/sandwich-jamon-queso.jpg', NULL, 5, 1),
(51, 'Palteado', 6, 'images/products/palteado.jpg', NULL, 5, 1),
(52, 'Sandwich de Salchicha Arequipeña', 8, 'images/products/sandwich-salchicha-arequipena.jpg', NULL, 5, 1),
(53, 'Croissant de Pollo', 8, 'images/products/croissant.jpg', NULL, 5, 1),
(54, 'Mango, Fresa y Pisco Peruano', 15, 'images/products/mango-fresa-pisco.jpg', NULL, 3, 1),
(55, 'Pera, Beterraga, Zumo de Limón y Pisco Peruano', 15, 'images/products/pera-beterraga-limon-pisco.jpg', NULL, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `receta`
--

CREATE TABLE `receta` (
  `id` int(11) NOT NULL,
  `quantity` float NOT NULL,
  `measure_unity` varchar(50) NOT NULL,
  `insumo_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `venta`
--

CREATE TABLE `venta` (
  `id` int(11) NOT NULL,
  `total_price` float NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(15) NOT NULL,
  `is_nulled` tinyint(1) DEFAULT NULL,
  `is_complain` tinyint(1) DEFAULT NULL,
  `complain_detail` text,
  `null_detail` text,
  `caja_id` int(11) NOT NULL,
  `descuento_id` int(11) DEFAULT NULL,
  `factura_RUC` varchar(11) DEFAULT NULL,
  `factura_razon_social` varchar(150) DEFAULT NULL,
  `order_name` varchar(150) DEFAULT NULL,
  `pay_amount` float NOT NULL,
  `pay_change` float NOT NULL,
  `cod_documento` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `venta`
--

INSERT INTO `venta` (`id`, `total_price`, `date`, `type`, `is_nulled`, `is_complain`, `complain_detail`, `null_detail`, `caja_id`, `descuento_id`, `factura_RUC`, `factura_razon_social`, `order_name`, `pay_amount`, `pay_change`, `cod_documento`) VALUES
(100, 7.9, '2018-03-28 18:08:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'aaa', 20, 12.1, 'B001-1'),
(101, 18.8, '2018-03-28 18:12:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Karen', 100, 81.2, 'B001-2'),
(102, 26.8, '2018-03-28 18:18:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'jesus', 100, 73.2, 'B001-3'),
(103, 5.9, '2018-03-28 18:33:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'jose', 50, 44.1, 'B001-4'),
(104, 25.7, '2018-03-28 18:35:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'LORENA', 50, 24.3, 'B001-5'),
(105, 5.9, '2018-03-28 18:37:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JESUS', 10, 4.1, 'B001-6'),
(106, 35.9, '2018-03-28 18:44:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MAURICIO CUPER', 50, 14.1, 'B001-7'),
(107, 18, '2018-03-28 18:47:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'SANDRA', 50, 32, 'B001-8'),
(108, 8, '2018-03-28 18:49:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'AYRTON', 8, 0, 'B001-9'),
(109, 37, '2018-03-28 18:53:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'KARLITA', 50, 13, 'B001-10'),
(110, 15, '2018-03-28 19:01:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'GERARDO', 50, 35, 'B001-11'),
(111, 18, '2018-03-28 19:03:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MELISA', 30, 12, 'B001-12'),
(112, 36.9, '2018-03-28 19:13:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'DIEGO', 50, 13.1, 'B001-13'),
(113, 10.9, '2018-03-28 19:21:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JACKELINE', 50, 39.1, 'B001-14'),
(114, 15, '2018-03-28 19:30:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'FRANCA', 20, 5, 'B001-15'),
(115, 15, '2018-03-28 19:30:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'FRANCA', 20, 5, 'B001-16'),
(116, 17.8, '2018-03-28 19:33:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'ANY - LLEVAR', 100, 82.2, 'B001-17'),
(117, 5.9, '2018-03-28 19:36:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'TERENICE', 10, 4.1, 'B001-18'),
(118, 36.9, '2018-03-28 19:42:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'GINO', 40, 3.1, 'B001-19'),
(119, 15, '2018-03-28 19:44:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Fabio', 16, 1, 'B001-20'),
(120, 15, '2018-03-28 19:49:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Claudia', 20, 5, 'B001-21'),
(121, 8, '2018-03-28 19:52:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'juan', 10, 2, 'B001-22'),
(122, 9.9, '2018-03-28 19:53:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Madeleine', 20, 10.1, 'B001-23'),
(123, 15, '2018-03-28 20:01:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'megeta', 100, 85, 'B001-24'),
(124, 11.8, '2018-03-28 20:03:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'GABY', 20, 8.2, 'B001-25'),
(125, 16.9, '2018-03-28 20:09:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'GABY', 20, 3.1, 'B001-26'),
(126, 15.9, '2018-03-28 20:10:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'ADITA', 20, 4.1, 'B001-27'),
(127, 35.8, '2018-03-28 20:12:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'IBETH', 40, 4.2, 'B001-28'),
(128, 12, '2018-03-28 20:13:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'GABY', 50, 38, 'B001-29'),
(129, 64.6, '2018-03-28 20:15:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JENNY', 100, 35.4, 'B001-30'),
(130, 5.9, '2018-03-28 20:19:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'AMELI', 20, 14.1, 'B001-31'),
(131, 30, '2018-03-28 20:24:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'winni', 100, 70, 'B001-32'),
(132, 30, '2018-03-28 20:26:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'carla', 50, 20, 'B001-33'),
(133, 5.9, '2018-03-28 20:29:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'kate', 20, 14.1, 'B001-34'),
(134, 15, '2018-03-28 20:30:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Rodrigo', 20, 5, 'B001-35'),
(135, 13.9, '2018-03-28 20:32:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'kristell', 20, 6.1, 'B001-36'),
(136, 39.9, '2018-03-28 20:33:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Jose', 50, 10.1, 'B001-37'),
(137, 14, '2018-03-28 20:35:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'danika', 50, 36, 'B001-38'),
(138, 16, '2018-03-28 20:36:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Maria', 50, 34, 'B001-39'),
(139, 8.9, '2018-03-28 20:40:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mALU', 20, 11.1, 'B001-40'),
(140, 6.9, '2018-03-28 20:40:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'sERGIO', 10, 3.1, 'B001-41'),
(141, 20, '2018-03-28 20:42:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JOSE LUIS', 20, 0, 'B001-42'),
(142, 35.9, '2018-03-28 20:43:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'rODRIGO', 40, 4.1, 'B001-43'),
(143, 30, '2018-03-28 20:46:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'carlos', 30, 0, 'B001-44'),
(144, 11.8, '2018-03-28 20:51:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'eliana', 20, 8.2, 'B001-45'),
(145, 22, '2018-03-28 21:18:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'david ', 100, 78, 'B001-46'),
(146, 30, '2018-03-28 21:20:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'marjo', 50, 20, 'B001-47'),
(147, 16.8, '2018-03-28 21:22:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'quico', 20, 3.2, 'B001-48'),
(148, 25.8, '2018-03-28 21:28:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'kathy - PARA LLEVAR', 100, 74.2, 'B001-49'),
(149, 20.9, '2018-03-28 21:30:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'RODRIGO', 50, 29.1, 'B001-50'),
(150, 30, '2018-03-28 21:33:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'EDU', 40, 10, 'B001-51'),
(151, 7, '2018-03-28 21:36:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JANDIR', 50, 43, 'B001-52'),
(152, 75, '2018-03-28 21:46:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'ANDRE', 100, 25, 'B001-53'),
(153, 15, '2018-03-28 22:03:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'CUBITOS', 50, 35, 'B001-54'),
(154, 16, '2018-03-28 22:05:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'ANDRE', 20, 4, 'B001-55'),
(155, 36, '2018-03-28 22:17:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'PIERO', 40, 4, 'B001-56'),
(156, 54.8, '2018-03-28 22:27:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'SUSANA', 100, 45.2, 'B001-57'),
(157, 23, '2018-03-28 22:35:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'CESAR', 100, 77, 'B001-58'),
(158, 23.9, '2018-03-28 22:38:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JUAN CARLOS', 30, 6.1, 'B001-59'),
(159, 15, '2018-03-28 22:42:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'RUBI', 15, 0, 'B001-60'),
(160, 7.9, '2018-03-29 09:22:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JOSE', 10, 2.1, 'B001-61'),
(161, 39.9, '2018-03-29 11:11:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'PABLO', 100, 60.1, 'B001-62'),
(162, 13.9, '2018-03-29 11:27:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MARIA', 100, 86.1, 'B001-63'),
(163, 15, '2018-03-29 11:29:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'DIEGO', 20, 5, 'B001-64'),
(164, 8.9, '2018-03-29 11:37:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'DEIGO', 9, 0.1, 'B001-65'),
(165, 8, '2018-03-29 12:41:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'CUIDANTE', 8, 0, 'B001-66'),
(166, 23.7, '2018-03-29 13:03:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'PAULINA', 30, 6.3, 'B001-67'),
(167, 11.8, '2018-03-29 14:16:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JAZMIN', 20, 8.2, 'B001-68'),
(168, 5.9, '2018-03-29 14:51:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'ANA', 6, 0.1, 'B001-69'),
(169, 30, '2018-03-29 15:45:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'PIETRO', 30, 0, 'B001-70'),
(170, 8.9, '2018-03-29 15:51:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'CLAUDIA', 8.9, 0, 'B001-71'),
(171, 23.8, '2018-03-29 17:49:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'YUMI', 30, 6.2, 'B001-72'),
(172, 11.8, '2018-03-29 18:01:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'CYNTHIA', 20, 8.2, 'B001-73'),
(173, 8, '2018-03-29 19:26:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'andres', 8, 0, 'B001-74'),
(174, 25.8, '2018-03-29 20:02:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'pamela', 26, 0.2, 'B001-75'),
(175, 15, '2018-03-29 20:28:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'pepe', 15, 0, 'B001-76'),
(176, 23.8, '2018-03-29 20:39:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'choo', 23.8, 0, 'B001-77'),
(177, 8, '2018-03-29 20:51:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'pp', 8, 0, 'B001-78'),
(178, 13.9, '2018-03-29 20:54:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'nicol', 20, 6.1, 'B001-79'),
(179, 13.9, '2018-03-29 20:55:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'fabiola', 15, 1.1, 'B001-80'),
(180, 45, '2018-03-29 21:19:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'gringo', 100, 55, 'B001-81'),
(181, 20.9, '2018-03-29 21:23:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'gerardo', 20.9, 0, 'B001-82'),
(182, 8.9, '2018-03-29 21:26:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'gian pier', 9, 0.1, 'B001-83'),
(183, 22.8, '2018-03-29 21:30:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'aaaa', 22.8, 0, 'B001-84'),
(184, 8, '2018-03-29 21:36:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'pp llevar', 10, 2, 'B001-85'),
(185, 9.9, '2018-03-29 21:54:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 1', 9.9, 0, 'B001-86'),
(186, 11.8, '2018-03-29 21:57:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'juanjo', 11.8, 0, 'B001-87'),
(187, 26, '2018-03-29 22:11:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 5', 26, 0, 'B001-88'),
(188, 7.9, '2018-03-29 22:47:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'francesa', 10, 2.1, 'B001-89'),
(189, 5, '2018-03-31 07:26:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Andres ', 5, 0, 'B001-90'),
(190, 5.9, '2018-03-31 09:07:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Sami', 5.9, 0, 'B001-91'),
(191, 19.9, '2018-03-31 09:20:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Alejandro', 19.9, 0, 'B001-92'),
(192, 8, '2018-03-31 10:01:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'ANDRES', 8, 0, 'B001-93'),
(193, 5.9, '2018-03-31 10:11:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'SONIA', 10, 4.1, 'B001-94'),
(194, 5.9, '2018-03-31 10:31:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'DANIEL', 10, 4.1, 'B001-95'),
(195, 11.9, '2018-03-31 10:35:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'VERONICA - PARA LLEVAR', 12, 0.1, 'B001-96'),
(196, 8, '2018-03-31 10:42:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Dilan', 20, 12, 'B001-97'),
(197, 15, '2018-03-31 11:47:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MARTHA', 15, 0, 'B001-98'),
(198, 13.9, '2018-03-31 12:06:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'GUILLERMO', 13.9, 0, 'B001-99'),
(199, 16, '2018-03-31 13:38:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'joel', 16, 0, 'B001-100'),
(200, 15, '2018-03-31 13:59:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'joel', 15, 0, 'B001-101'),
(201, 19.8, '2018-03-31 14:00:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 2', 19.8, 0, 'B001-102'),
(202, 18.8, '2018-03-31 14:31:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa1', 100, 81.2, 'B001-103'),
(203, 19.8, '2018-03-31 14:37:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 2', 20, 0.2, 'B001-104'),
(204, 5.9, '2018-03-31 15:09:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa1', 6, 0.1, 'B001-105'),
(205, 30, '2018-03-31 15:51:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 2', 30, 0, 'B001-106'),
(206, 17.8, '2018-03-31 16:06:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'margut', 50, 32.2, 'B001-107'),
(207, 5.9, '2018-03-31 16:24:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'fabian', 5.9, 0, 'B001-108'),
(208, 50.7, '2018-03-31 17:21:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'rizal', 50.7, 0, 'B001-109'),
(209, 8.9, '2018-03-31 17:35:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'atusa', 20, 11.1, 'B001-110'),
(210, 20.9, '2018-03-31 18:25:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 4', 20.9, 0, 'B001-111'),
(211, 32.9, '2018-03-31 18:45:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 1', 32.9, 0, 'B001-112'),
(212, 15, '2018-03-31 18:55:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'PESH', 20, 5, 'B001-113'),
(213, 13.9, '2018-03-31 19:04:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA3', 13.9, 0, 'B001-114'),
(214, 35.9, '2018-03-31 19:31:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'PERCI', 35.9, 0, 'B001-115'),
(215, 30, '2018-03-31 19:32:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 30, 0, 'B001-116'),
(216, 15, '2018-03-31 19:33:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'PERCY', 15, 0, 'B001-117'),
(217, 8, '2018-03-31 19:47:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 8, 0, 'B001-118'),
(218, 8, '2018-03-31 19:55:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 8, 0, 'B001-119'),
(219, 18.9, '2018-03-31 21:30:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JOVANI', 20, 1.1, 'B001-120'),
(220, 26.8, '2018-03-31 21:32:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 3', 26.8, 0, 'B001-121'),
(221, 5.9, '2018-04-02 08:04:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'melany', 20, 14.1, 'B001-122'),
(222, 13.9, '2018-04-02 09:10:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'jesus', 100, 86.1, 'B001-123'),
(223, 13.9, '2018-04-02 09:37:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'PIERO', 20, 6.1, 'B001-124'),
(224, 17.9, '2018-04-02 10:13:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 1', 17.9, 0, 'B001-125'),
(225, 13.9, '2018-04-02 10:38:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Victor Cuper', 20, 6.1, 'B001-126'),
(226, 37.7, '2018-04-02 10:46:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 4', 37.7, 0, 'B001-127'),
(227, 15, '2018-04-02 11:50:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 2', 15, 0, 'B001-128'),
(228, 4.5, '2018-04-02 12:14:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'ELITA', 4.5, 0, 'B001-129'),
(229, 5.9, '2018-04-02 12:36:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'TERE', 5.9, 0, 'B001-130'),
(230, 13.9, '2018-04-02 15:01:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Cliente', 13.9, 0, 'B001-131'),
(231, 18.8, '2018-04-02 15:16:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Rosio', 20, 1.2, 'B001-132'),
(232, 5.9, '2018-04-02 16:55:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Nary', 20, 14.1, 'B001-133'),
(233, 6, '2018-04-02 17:18:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JIMENA', 20, 14, 'B001-134'),
(234, 4.5, '2018-04-02 17:19:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MELCHOR', 4.5, 0, 'B001-135'),
(235, 8, '2018-04-02 17:36:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'BARRA', 8, 0, 'B001-136'),
(236, 5.9, '2018-04-02 18:16:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'CLAUDIA', 5.9, 0, 'B001-137'),
(237, 8, '2018-04-02 18:16:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JOSE', 8, 0, 'B001-138'),
(238, 23.6, '2018-04-02 18:19:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 23.6, 0, 'B001-139'),
(239, 6, '2018-04-02 18:20:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'CLAUDIA', 6, 0, 'B001-140'),
(240, 13.9, '2018-04-02 18:40:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Claudia', 13.9, 0, 'B001-141'),
(241, 5.9, '2018-04-02 18:41:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'barra 3', 5.9, 0, 'B001-142'),
(242, 24.9, '2018-04-02 20:05:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 2', 24.9, 0, 'B001-143'),
(243, 4.5, '2018-04-02 20:05:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'SONIA', 10, 5.5, 'B001-144'),
(244, 5, '2018-04-02 20:32:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'ANDRES', 5, 0, 'B001-145'),
(245, 44.8, '2018-04-02 21:41:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'ELINA', 50, 5.2, 'B001-146'),
(246, 8, '2018-04-02 22:11:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'paolo', 10, 2, 'B001-147'),
(247, 25.8, '2018-04-03 09:09:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 1', 25.8, 0, 'B001-148'),
(248, 8.9, '2018-04-03 10:05:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'elizabeth', 50, 41.1, 'B001-149'),
(249, 17.8, '2018-04-03 10:19:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA ', 17.8, 0, 'B001-150'),
(251, 5.9, '2018-04-03 10:40:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'DIANA', 5.9, 0, 'B001-151'),
(252, 64.2, '2018-04-03 11:00:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'FRANCA', 100, 35.8, 'B001-152'),
(253, 29.8, '2018-04-03 11:20:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 2', 29.8, 0, 'B001-153'),
(254, 23.8, '2018-04-03 11:22:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'victor cuper', 100, 76.2, 'B001-154'),
(255, 37.9, '2018-04-03 11:47:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 4', 50, 12.1, 'B001-155'),
(256, 16.9, '2018-04-03 12:23:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'CLIENTE', 20, 3.1, 'B001-156'),
(257, 9.9, '2018-04-03 12:49:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'anna', 10, 0.1, 'B001-157'),
(258, 30, '2018-04-03 12:52:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'LUCIA', 100, 70, 'B001-158'),
(260, 7.9, '2018-04-03 13:50:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'barra', 50, 42.1, 'B001-159'),
(261, 8.9, '2018-04-03 15:10:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Alice', 8.9, 0, 'B001-160'),
(262, 5.9, '2018-04-03 16:29:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 2', 5.9, 0, 'B001-161'),
(263, 4.5, '2018-04-03 18:04:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Rosa', 5, 0.5, 'B001-162'),
(264, 20.9, '2018-04-03 18:15:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 20.9, 0, 'B001-163'),
(265, 13.9, '2018-04-03 19:29:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 3', 14, 0.1, 'B001-164'),
(266, 31.9, '2018-04-03 20:00:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'CESAR', 31.9, 0, 'B001-165'),
(267, 11.8, '2018-04-03 20:06:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 6', 11.8, 0, 'B001-166'),
(268, 17.8, '2018-04-03 20:16:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JAVIER', 17.8, 0, 'B001-167'),
(269, 7.9, '2018-04-03 20:26:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 2', 7.9, 0, 'B001-168'),
(271, 19.9, '2018-04-03 20:49:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'betzy', 19.9, 0, 'B001-169'),
(272, 11.8, '2018-04-03 20:58:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 3', 11.8, 0, 'B001-170'),
(273, 55.6, '2018-04-03 21:04:00', 'factura', 0, 0, '', '', 1, NULL, '20543052907', 'INNOVA GOVERNMENT E.I.R.L.', 'MESA 1', 55.6, 0, 'F001-1'),
(274, 19, '2018-04-03 22:03:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'lucho', 19, 0, 'B001-171'),
(275, 9.9, '2018-04-04 10:05:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'BARRA', 9.9, 0, 'B001-172'),
(276, 49.4, '2018-04-04 10:11:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'BCP - DELIVERY', 49.4, 0, 'B001-173'),
(277, 17.8, '2018-04-04 10:14:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JESUS', 17.8, 0, 'B001-174'),
(278, 19.8, '2018-04-04 11:29:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Cliente ', 19.8, 0, 'B001-175'),
(279, 11.8, '2018-04-04 11:36:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'hugo', 11.8, 0, 'B001-176'),
(280, 17.8, '2018-04-04 11:55:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 1 ', 17.8, 0, 'B001-177'),
(281, 12, '2018-04-04 12:10:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 2', 12, 0, 'B001-178'),
(282, 12, '2018-04-04 12:26:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Cliente', 12, 0, 'B001-179'),
(283, 8, '2018-04-04 13:56:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'andres', 10, 2, 'B001-180'),
(284, 17.8, '2018-04-04 14:22:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'cliente', 20, 2.2, 'B001-181'),
(285, 19.8, '2018-04-04 14:25:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Cliente', 20, 0.2, 'B001-182'),
(286, 8.9, '2018-04-04 14:40:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'cliente', 20, 11.1, 'B001-183'),
(287, 15, '2018-04-04 15:38:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Alan', 15, 0, 'B001-184'),
(288, 5.9, '2018-04-04 16:06:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'samy', 10, 4.1, 'B001-185'),
(289, 16.9, '2018-04-04 16:13:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Maricarmen', 200, 183.1, 'B001-186'),
(290, 30, '2018-04-04 16:35:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Emil', 30, 0, 'B001-187'),
(291, 16.8, '2018-04-04 17:15:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 3', 16.8, 0, 'B001-188'),
(294, 8.9, '2018-04-04 18:49:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 8.9, 0, 'B001-189'),
(295, 40.3, '2018-04-04 19:12:00', 'factura', 0, 0, '', '', 1, NULL, '20543052907', 'INNOVA GOVERNMENT E.I.R.L.', 'AAAA', 40.3, 0, 'F001-2'),
(296, 21.8, '2018-04-04 19:41:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'SALOMON', 25, 3.2, 'B001-190'),
(297, 18.8, '2018-04-04 20:22:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 6', 18.8, 0, 'B001-191'),
(298, 44.7, '2018-04-04 20:55:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 44.7, 0, 'B001-192'),
(299, 195.7, '2018-04-04 20:58:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'KARINA', 195.7, -0, 'B001-193'),
(300, 34.4, '2018-04-04 21:40:00', 'factura', 0, 0, '', '', 1, NULL, '20418140551', 'ALBIS S.A.', 'MESA', 34.4, 0, 'F001-3'),
(301, 26.7, '2018-04-05 10:50:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'victor', 26.7, -0, 'B001-194'),
(302, 13, '2018-04-05 11:44:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'andres', 20, 7, 'B001-195'),
(303, 12, '2018-04-05 11:59:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'BARRA', 15, 3, 'B001-196'),
(304, 8, '2018-04-05 12:05:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'SANDRA', 10, 2, 'B001-197'),
(305, 20.9, '2018-04-05 12:21:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'SANDRITA', 21, 0.1, 'B001-198'),
(306, 6, '2018-04-05 12:38:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MAMA MAGALY', 10, 4, 'B001-199'),
(307, 8, '2018-04-05 12:44:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'ANDRES', 8, 0, 'B001-200'),
(308, 7.9, '2018-04-05 13:04:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'BARRA ', 10, 2.1, 'B001-201'),
(309, 21, '2018-04-05 13:11:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 2', 21, 0, 'B001-202'),
(310, 5, '2018-04-05 13:48:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Cliente', 5, 0, 'B001-203'),
(311, 8.9, '2018-04-05 14:55:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Nicco', 10, 1.1, 'B001-204'),
(312, 23.8, '2018-04-05 15:46:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Owen', 50, 26.2, 'B001-205'),
(313, 13.9, '2018-04-05 15:52:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Frida', 20, 6.1, 'B001-206'),
(314, 15.8, '2018-04-05 16:17:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 2', 15.8, 0, 'B001-207'),
(315, 11.8, '2018-04-05 16:30:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'SUSAN', 11.8, 0, 'B001-208'),
(316, 33.8, '2018-04-05 17:18:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 3', 33.8, 0, 'B001-209'),
(317, 22, '2018-04-05 18:50:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 5', 22, 0, 'B001-210'),
(318, 24.9, '2018-04-05 19:22:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 3', 24.9, 0, 'B001-211'),
(319, 8, '2018-04-05 19:23:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'FIORELLA - LLEVAR', 8, 0, 'B001-212'),
(320, 17.8, '2018-04-05 19:32:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 1', 17.8, 0, 'B001-213'),
(321, 8, '2018-04-05 20:26:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 20, 12, 'B001-214'),
(323, 30, '2018-04-05 20:38:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'paulo', 100, 70, 'B001-215'),
(324, 45, '2018-04-05 21:36:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 2', 45, 0, 'B001-216'),
(325, 9.9, '2018-04-05 21:39:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'barra', 10, 0.1, 'B001-217'),
(326, 9.9, '2018-04-05 21:41:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 1', 50, 40.1, 'B001-218'),
(327, 15, '2018-04-05 21:44:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'piero', 20, 5, 'B001-219'),
(328, 44.7, '2018-04-06 10:37:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MAURICIO CUPER', 50, 5.3, 'B001-220'),
(329, 64.5, '2018-04-06 10:43:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'paulo', 100, 35.5, 'B001-221'),
(330, 5, '2018-04-06 11:28:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Cliente', 20, 15, 'B001-222'),
(331, 15, '2018-04-06 12:29:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'PATRICIA SEGURA - DELIVERY', 15, 0, 'B001-223'),
(332, 7.9, '2018-04-06 12:30:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 2', 7.9, 0, 'B001-224'),
(333, 7.9, '2018-04-06 14:11:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'martin', 100, 92.1, 'B001-225'),
(334, 13.9, '2018-04-06 14:57:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'sandra', 20, 6.1, 'B001-226'),
(335, 30, '2018-04-06 16:12:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Pamela', 50, 20, 'B001-227'),
(336, 41.7, '2018-04-06 18:55:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 41.7, 0, 'B001-228'),
(337, 5.9, '2018-04-06 19:38:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 2', 5.9, 0, 'B001-229'),
(338, 29.8, '2018-04-06 19:46:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 29.8, 0, 'B001-230'),
(339, 17.4, '2018-04-06 19:53:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 3', 17.4, 0, 'B001-231'),
(341, 26.8, '2018-04-06 21:22:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'ELIZABETH', 50, 23.2, 'B001-232'),
(342, 25, '2018-04-06 21:53:00', 'factura', 0, 0, '', '', 1, NULL, '20498189637', 'AREQUIPA EXPRESO MARVISUR EIRL', 'MESA 3', 50, 25, 'F001-4'),
(343, 5.9, '2018-04-07 08:29:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Sofia', 6, 0.1, 'B001-233'),
(344, 11.8, '2018-04-07 10:39:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MIRIAN', 11.8, 0, 'B001-234'),
(345, 39.9, '2018-04-07 11:27:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'PAULO', 39.9, 0, 'B001-235'),
(346, 5.9, '2018-04-07 11:41:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'BARRA - LLEVAR', 5.9, 0, 'B001-236'),
(347, 15.8, '2018-04-07 12:23:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 3', 15.8, 0, 'B001-237'),
(348, 39.7, '2018-04-07 12:24:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 39.7, 0, 'B001-238'),
(349, 8.9, '2018-04-07 12:48:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'KAREN VENTANILLA 8 INTERBANK', 8.9, 0, 'B001-239'),
(350, 9.9, '2018-04-07 12:53:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 4', 9.9, 0, 'B001-240'),
(351, 24.9, '2018-04-07 16:48:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 4', 24.9, 0, 'B001-241'),
(352, 19.8, '2018-04-07 16:49:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 3', 19.8, 0, 'B001-242'),
(353, 5.9, '2018-04-07 16:50:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'JUAN - LLEVAR', 5.9, 0, 'B001-243'),
(354, 22.9, '2018-04-07 18:47:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 3', 22.9, 0, 'B001-244'),
(355, 23.9, '2018-04-07 18:53:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA 2', 23.9, 0, 'B001-245'),
(356, 9.9, '2018-04-07 18:59:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MIRIAN - LLEVAR', 9.9, 0, 'B001-246'),
(358, 33.9, '2018-04-07 19:34:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 4', 33.9, 0, 'B001-247'),
(359, 24.8, '2018-04-07 19:54:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 5', 24.8, 0, 'B001-248'),
(360, 11.8, '2018-04-07 21:10:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 1', 20, 8.2, 'B001-249'),
(361, 15.8, '2018-04-07 21:22:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 3', 15.8, 0, 'B001-250'),
(362, 8, '2018-04-07 22:46:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 2', 8, 0, 'B001-251'),
(363, 58.9, '2018-04-07 22:56:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 5', 58.9, 0, 'B001-252'),
(364, 5.9, '2018-04-09 10:24:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Cliente', 10, 4.1, 'B001-253'),
(365, 47.2, '2018-04-09 10:53:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'BCP', 47.2, 0, 'B001-254'),
(366, 15.9, '2018-04-09 12:22:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Cliente', 15.9, 0, 'B001-255'),
(367, 12.9, '2018-04-09 12:23:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Cliente', 13, 0.1, 'B001-256'),
(368, 9.9, '2018-04-09 13:26:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'andres ', 10, 0.1, 'B001-257'),
(369, 9, '2018-04-09 14:57:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'MESA', 50, 41, 'B001-258'),
(371, 5.9, '2018-04-09 16:30:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Wiliam', 20, 14.1, 'B001-259'),
(372, 11.8, '2018-04-09 16:52:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'mesa 1', 11.8, 0, 'B001-260'),
(373, 5.9, '2018-04-09 17:10:00', 'boleta', 0, 0, '', '', 1, NULL, NULL, NULL, 'Mesa 5', 5.9, 0, 'B001-261');

-- --------------------------------------------------------

--
-- Table structure for table `venta_producto`
--

CREATE TABLE `venta_producto` (
  `id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `venta_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `venta_producto`
--

INSERT INTO `venta_producto` (`id`, `amount`, `producto_id`, `venta_id`) VALUES
(71, 1, 14, 100),
(72, 1, 2, 101),
(73, 1, 46, 101),
(74, 1, 2, 102),
(75, 1, 46, 102),
(76, 1, 52, 102),
(77, 1, 7, 103),
(78, 1, 13, 104),
(79, 1, 46, 104),
(80, 1, 47, 104),
(81, 1, 7, 105),
(82, 1, 13, 106),
(83, 1, 34, 106),
(84, 1, 50, 106),
(85, 1, 52, 106),
(86, 1, 41, 107),
(87, 1, 53, 107),
(88, 1, 52, 108),
(89, 2, 34, 109),
(90, 1, 50, 109),
(91, 1, 34, 110),
(92, 1, 40, 111),
(93, 1, 53, 111),
(94, 1, 11, 112),
(95, 1, 39, 112),
(96, 1, 52, 112),
(97, 1, 53, 112),
(98, 1, 3, 113),
(99, 1, 48, 113),
(100, 1, 34, 114),
(101, 1, 38, 115),
(102, 1, 4, 116),
(103, 1, 8, 116),
(104, 1, 5, 117),
(105, 1, 13, 118),
(106, 1, 34, 118),
(107, 1, 52, 118),
(108, 1, 53, 118),
(109, 1, 34, 119),
(110, 1, 38, 120),
(111, 1, 53, 121),
(112, 1, 47, 122),
(113, 1, 35, 123),
(114, 1, 7, 124),
(115, 1, 9, 124),
(116, 1, 8, 125),
(117, 1, 53, 125),
(118, 1, 2, 126),
(119, 1, 50, 126),
(120, 2, 47, 127),
(121, 2, 53, 127),
(122, 1, 48, 128),
(123, 1, 50, 128),
(124, 1, 4, 129),
(125, 1, 8, 129),
(126, 1, 10, 129),
(127, 1, 12, 129),
(128, 1, 48, 129),
(129, 1, 49, 129),
(130, 2, 52, 129),
(131, 1, 1, 130),
(132, 1, 34, 131),
(133, 1, 54, 131),
(134, 1, 34, 132),
(135, 1, 43, 132),
(136, 1, 1, 133),
(137, 1, 35, 134),
(138, 1, 11, 135),
(139, 1, 52, 135),
(140, 2, 40, 136),
(141, 1, 47, 136),
(142, 2, 50, 137),
(143, 2, 52, 138),
(144, 1, 4, 139),
(145, 1, 31, 140),
(146, 1, 40, 141),
(147, 1, 48, 141),
(148, 1, 9, 142),
(149, 1, 34, 142),
(150, 1, 42, 142),
(151, 1, 39, 143),
(152, 1, 54, 143),
(153, 1, 5, 144),
(154, 1, 7, 144),
(155, 1, 35, 145),
(156, 1, 50, 145),
(157, 2, 39, 146),
(158, 1, 14, 147),
(159, 1, 24, 147),
(160, 1, 13, 148),
(161, 1, 15, 148),
(162, 2, 50, 148),
(163, 1, 11, 149),
(164, 1, 40, 149),
(165, 2, 54, 150),
(166, 1, 50, 151),
(167, 2, 35, 152),
(168, 1, 39, 152),
(169, 1, 54, 152),
(170, 1, 55, 152),
(171, 1, 54, 153),
(172, 2, 49, 154),
(173, 1, 42, 155),
(174, 1, 51, 155),
(175, 1, 55, 155),
(176, 1, 2, 156),
(177, 1, 33, 156),
(178, 1, 38, 156),
(179, 1, 41, 156),
(180, 1, 50, 156),
(181, 1, 38, 157),
(182, 1, 53, 157),
(183, 1, 2, 158),
(184, 1, 50, 158),
(185, 1, 53, 158),
(186, 1, 55, 159),
(187, 1, 14, 160),
(188, 1, 4, 161),
(189, 1, 40, 161),
(190, 2, 52, 161),
(191, 1, 9, 162),
(192, 1, 52, 162),
(193, 1, 44, 163),
(194, 1, 2, 164),
(195, 1, 53, 165),
(196, 1, 5, 166),
(197, 2, 6, 166),
(198, 1, 1, 167),
(199, 1, 7, 167),
(200, 1, 1, 168),
(201, 1, 40, 169),
(202, 1, 43, 169),
(203, 1, 4, 170),
(204, 1, 8, 171),
(205, 1, 46, 171),
(206, 1, 48, 171),
(207, 1, 1, 172),
(208, 1, 15, 172),
(209, 1, 52, 173),
(210, 2, 6, 174),
(211, 1, 52, 174),
(212, 1, 34, 175),
(213, 1, 1, 176),
(214, 1, 5, 176),
(215, 1, 48, 176),
(216, 1, 50, 176),
(217, 1, 52, 177),
(218, 1, 5, 178),
(219, 1, 53, 178),
(220, 1, 17, 179),
(221, 1, 49, 179),
(222, 1, 35, 180),
(223, 2, 38, 180),
(224, 1, 5, 181),
(225, 1, 42, 181),
(226, 1, 12, 182),
(227, 1, 1, 183),
(228, 1, 9, 183),
(229, 1, 48, 183),
(230, 1, 51, 183),
(231, 1, 49, 184),
(232, 1, 46, 185),
(233, 1, 1, 186),
(234, 1, 3, 186),
(235, 1, 43, 187),
(236, 1, 48, 187),
(237, 1, 51, 187),
(238, 1, 22, 188),
(239, 1, 48, 189),
(240, 1, 1, 190),
(241, 1, 11, 191),
(242, 2, 50, 191),
(243, 1, 53, 192),
(244, 1, 5, 193),
(245, 1, 15, 194),
(246, 1, 23, 195),
(247, 1, 48, 195),
(248, 1, 52, 196),
(249, 1, 34, 197),
(250, 1, 17, 198),
(251, 1, 53, 198),
(252, 1, 52, 199),
(253, 1, 53, 199),
(254, 1, 40, 200),
(255, 1, 46, 201),
(256, 1, 47, 201),
(257, 1, 8, 202),
(258, 1, 47, 202),
(259, 1, 46, 203),
(260, 1, 47, 203),
(261, 1, 1, 204),
(262, 1, 43, 205),
(263, 1, 44, 205),
(264, 2, 6, 206),
(265, 1, 19, 207),
(266, 1, 8, 208),
(267, 1, 12, 208),
(268, 1, 40, 208),
(269, 1, 46, 208),
(270, 1, 53, 208),
(271, 1, 10, 209),
(272, 1, 5, 210),
(273, 1, 44, 210),
(274, 1, 3, 211),
(275, 1, 41, 211),
(276, 2, 51, 211),
(277, 1, 43, 212),
(278, 1, 9, 213),
(279, 1, 49, 213),
(280, 1, 7, 214),
(281, 1, 40, 214),
(282, 1, 50, 214),
(283, 1, 53, 214),
(284, 1, 40, 215),
(285, 1, 43, 215),
(286, 1, 38, 216),
(287, 1, 53, 217),
(288, 1, 52, 218),
(289, 1, 1, 219),
(290, 1, 48, 219),
(291, 1, 53, 219),
(292, 1, 6, 220),
(293, 1, 47, 220),
(294, 1, 49, 220),
(295, 1, 15, 221),
(296, 1, 9, 222),
(297, 1, 52, 222),
(298, 1, 5, 223),
(299, 1, 52, 223),
(300, 1, 9, 224),
(301, 2, 51, 224),
(302, 1, 3, 225),
(303, 1, 53, 225),
(304, 2, 3, 226),
(305, 1, 23, 226),
(306, 1, 48, 226),
(307, 2, 50, 226),
(308, 1, 40, 227),
(309, 1, 33, 228),
(310, 1, 11, 229),
(311, 1, 6, 230),
(312, 1, 48, 230),
(313, 1, 8, 231),
(314, 1, 46, 231),
(315, 1, 1, 232),
(316, 1, 51, 233),
(317, 1, 33, 234),
(318, 1, 52, 235),
(319, 1, 11, 236),
(320, 1, 53, 237),
(321, 1, 1, 238),
(322, 2, 3, 238),
(323, 1, 7, 238),
(324, 1, 51, 239),
(325, 1, 3, 240),
(326, 1, 52, 240),
(327, 1, 3, 241),
(328, 1, 43, 242),
(329, 1, 47, 242),
(330, 1, 33, 243),
(331, 1, 48, 244),
(332, 1, 2, 245),
(333, 1, 8, 245),
(334, 1, 44, 245),
(335, 2, 51, 245),
(336, 1, 49, 246),
(337, 2, 46, 247),
(338, 1, 51, 247),
(339, 1, 6, 248),
(340, 2, 10, 249),
(344, 1, 7, 251),
(345, 1, 4, 252),
(346, 1, 13, 252),
(347, 1, 14, 252),
(348, 1, 33, 252),
(349, 1, 48, 252),
(350, 2, 52, 252),
(351, 2, 53, 252),
(352, 1, 4, 253),
(353, 1, 6, 253),
(354, 2, 51, 253),
(355, 1, 1, 254),
(356, 1, 47, 254),
(357, 1, 53, 254),
(358, 1, 16, 255),
(359, 1, 41, 255),
(360, 1, 42, 255),
(361, 1, 8, 256),
(362, 1, 52, 256),
(363, 1, 47, 257),
(364, 1, 40, 258),
(365, 1, 43, 258),
(367, 1, 14, 260),
(368, 1, 6, 261),
(369, 1, 3, 262),
(370, 1, 33, 263),
(371, 1, 15, 264),
(372, 1, 38, 264),
(373, 1, 7, 265),
(374, 1, 53, 265),
(375, 1, 10, 266),
(376, 1, 44, 266),
(377, 1, 52, 266),
(378, 2, 1, 267),
(379, 1, 6, 268),
(380, 1, 8, 268),
(381, 1, 16, 269),
(386, 1, 24, 271),
(387, 1, 48, 271),
(388, 1, 51, 271),
(389, 1, 1, 272),
(390, 1, 21, 272),
(391, 2, 5, 273),
(392, 2, 15, 273),
(393, 2, 49, 273),
(394, 2, 53, 273),
(395, 1, 50, 274),
(396, 2, 51, 274),
(397, 1, 46, 275),
(398, 1, 16, 276),
(399, 1, 33, 276),
(400, 1, 48, 276),
(401, 2, 49, 276),
(402, 1, 52, 276),
(403, 1, 53, 276),
(404, 2, 12, 277),
(405, 1, 46, 278),
(406, 1, 47, 278),
(407, 1, 1, 279),
(408, 1, 3, 279),
(409, 1, 18, 280),
(410, 1, 47, 280),
(411, 2, 51, 281),
(412, 2, 51, 282),
(413, 1, 52, 283),
(414, 2, 2, 284),
(415, 2, 11, 285),
(416, 1, 52, 285),
(417, 1, 2, 286),
(418, 1, 35, 287),
(419, 1, 13, 288),
(420, 1, 24, 289),
(421, 1, 52, 289),
(422, 2, 40, 290),
(423, 1, 6, 291),
(424, 1, 16, 291),
(430, 1, 8, 294),
(431, 1, 19, 295),
(432, 1, 23, 295),
(433, 1, 33, 295),
(434, 1, 49, 295),
(435, 1, 50, 295),
(436, 1, 53, 295),
(437, 1, 15, 296),
(438, 1, 24, 296),
(439, 1, 50, 296),
(440, 1, 7, 297),
(441, 1, 9, 297),
(442, 1, 50, 297),
(443, 1, 4, 298),
(444, 2, 46, 298),
(445, 2, 49, 298),
(446, 1, 7, 299),
(447, 2, 11, 299),
(448, 1, 35, 299),
(449, 1, 40, 299),
(450, 2, 42, 299),
(451, 3, 43, 299),
(452, 1, 48, 299),
(453, 1, 49, 299),
(454, 2, 50, 299),
(455, 1, 51, 299),
(456, 1, 52, 299),
(457, 4, 53, 299),
(458, 1, 16, 300),
(459, 1, 33, 300),
(460, 1, 40, 300),
(461, 1, 50, 300),
(462, 2, 13, 301),
(463, 1, 47, 301),
(464, 1, 48, 301),
(465, 1, 48, 302),
(466, 1, 52, 302),
(467, 2, 51, 303),
(468, 1, 53, 304),
(469, 1, 3, 305),
(470, 1, 44, 305),
(471, 1, 51, 306),
(472, 1, 49, 307),
(473, 1, 22, 308),
(474, 1, 45, 309),
(475, 1, 51, 309),
(476, 1, 48, 310),
(477, 1, 32, 311),
(478, 1, 15, 312),
(479, 1, 47, 312),
(480, 1, 52, 312),
(481, 1, 5, 313),
(482, 1, 53, 313),
(483, 1, 11, 314),
(484, 1, 46, 314),
(485, 1, 5, 315),
(486, 1, 11, 315),
(487, 1, 4, 316),
(488, 1, 12, 316),
(489, 2, 53, 316),
(490, 1, 43, 317),
(491, 1, 50, 317),
(492, 1, 34, 318),
(493, 1, 47, 318),
(494, 1, 53, 319),
(495, 1, 2, 320),
(496, 1, 6, 320),
(497, 1, 53, 321),
(501, 1, 38, 323),
(502, 1, 39, 323),
(503, 1, 35, 324),
(504, 1, 50, 324),
(505, 1, 52, 324),
(506, 1, 55, 324),
(507, 1, 47, 325),
(508, 1, 47, 326),
(509, 1, 40, 327),
(510, 1, 24, 328),
(511, 2, 47, 328),
(512, 2, 53, 328),
(513, 1, 6, 329),
(514, 4, 47, 329),
(515, 2, 53, 329),
(516, 1, 48, 330),
(517, 1, 40, 331),
(518, 1, 14, 332),
(519, 1, 14, 333),
(520, 1, 11, 334),
(521, 1, 53, 334),
(522, 1, 34, 335),
(523, 1, 39, 335),
(524, 1, 18, 336),
(525, 2, 24, 336),
(526, 2, 52, 336),
(527, 1, 17, 337),
(528, 2, 26, 338),
(529, 2, 51, 338),
(530, 1, 9, 339),
(531, 1, 33, 339),
(532, 1, 50, 339),
(535, 2, 23, 341),
(536, 1, 48, 341),
(537, 1, 53, 341),
(538, 2, 33, 342),
(539, 2, 52, 342),
(540, 1, 15, 343),
(541, 1, 1, 344),
(542, 1, 7, 344),
(543, 1, 6, 345),
(544, 1, 43, 345),
(545, 1, 52, 345),
(546, 1, 53, 345),
(547, 1, 3, 346),
(548, 1, 17, 347),
(549, 1, 46, 347),
(550, 1, 1, 348),
(551, 1, 11, 348),
(552, 1, 17, 348),
(553, 1, 51, 348),
(554, 1, 52, 348),
(555, 1, 53, 348),
(556, 1, 10, 349),
(557, 1, 47, 350),
(558, 1, 40, 351),
(559, 1, 46, 351),
(560, 2, 46, 352),
(561, 1, 5, 353),
(562, 1, 18, 354),
(563, 1, 34, 354),
(564, 1, 24, 355),
(565, 1, 40, 355),
(566, 1, 47, 356),
(569, 1, 11, 358),
(570, 1, 43, 358),
(571, 1, 50, 358),
(572, 1, 51, 358),
(573, 1, 8, 359),
(574, 1, 11, 359),
(575, 2, 48, 359),
(576, 1, 5, 360),
(577, 1, 7, 360),
(578, 1, 11, 361),
(579, 1, 47, 361),
(580, 1, 53, 362),
(581, 1, 17, 363),
(582, 3, 38, 363),
(583, 1, 52, 363),
(584, 1, 15, 364),
(585, 1, 33, 365),
(586, 3, 47, 365),
(587, 1, 48, 365),
(588, 1, 53, 365),
(589, 1, 10, 366),
(590, 1, 50, 366),
(591, 1, 7, 367),
(592, 1, 50, 367),
(593, 1, 47, 368),
(594, 2, 33, 369),
(596, 1, 15, 371),
(597, 2, 3, 372),
(598, 1, 15, 373);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `caja_empleado`
--
ALTER TABLE `caja_empleado`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_empleado_caja_id` (`empleado_id`),
  ADD KEY `fk_caja_id` (`caja_id`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `descuentos`
--
ALTER TABLE `descuentos`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `insumo`
--
ALTER TABLE `insumo`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_producto_descuento_id` (`descuento_id`),
  ADD KEY `fk_categoria_id` (`categoria_id`);

--
-- Indexes for table `receta`
--
ALTER TABLE `receta`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_insumo_id` (`insumo_id`),
  ADD KEY `fk_producto_id` (`producto_id`);

--
-- Indexes for table `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_venta_descuento_id` (`descuento_id`),
  ADD KEY `fk_venta_caja_id` (`caja_id`);

--
-- Indexes for table `venta_producto`
--
ALTER TABLE `venta_producto`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_venta_producto_id` (`producto_id`),
  ADD KEY `fk_venta_id` (`venta_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `caja`
--
ALTER TABLE `caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `caja_empleado`
--
ALTER TABLE `caja_empleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `descuentos`
--
ALTER TABLE `descuentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `insumo`
--
ALTER TABLE `insumo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `receta`
--
ALTER TABLE `receta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `venta`
--
ALTER TABLE `venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=374;

--
-- AUTO_INCREMENT for table `venta_producto`
--
ALTER TABLE `venta_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=599;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `caja_empleado`
--
ALTER TABLE `caja_empleado`
  ADD CONSTRAINT `fk_caja_id` FOREIGN KEY (`caja_id`) REFERENCES `caja` (`id`),
  ADD CONSTRAINT `fk_empleado_caja_id` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`id`);

--
-- Constraints for table `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_categoria_id` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`),
  ADD CONSTRAINT `fk_producto_descuento_id` FOREIGN KEY (`descuento_id`) REFERENCES `descuentos` (`id`);

--
-- Constraints for table `receta`
--
ALTER TABLE `receta`
  ADD CONSTRAINT `fk_insumo_id` FOREIGN KEY (`insumo_id`) REFERENCES `insumo` (`id`),
  ADD CONSTRAINT `fk_producto_id` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`);

--
-- Constraints for table `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `fk_venta_caja_id` FOREIGN KEY (`caja_id`) REFERENCES `caja` (`id`),
  ADD CONSTRAINT `fk_venta_descuento_id` FOREIGN KEY (`descuento_id`) REFERENCES `descuentos` (`id`);

--
-- Constraints for table `venta_producto`
--
ALTER TABLE `venta_producto`
  ADD CONSTRAINT `fk_venta_id` FOREIGN KEY (`venta_id`) REFERENCES `venta` (`id`),
  ADD CONSTRAINT `fk_venta_producto_id` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
