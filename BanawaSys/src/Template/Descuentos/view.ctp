<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Descuento $descuento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Descuento'), ['action' => 'edit', $descuento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Descuento'), ['action' => 'delete', $descuento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $descuento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Descuentos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Descuento'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Producto'), ['controller' => 'Producto', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Producto', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Venta'), ['controller' => 'Venta', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Venta'), ['controller' => 'Venta', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="descuentos view large-9 medium-8 columns content">
    <h3><?= h($descuento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($descuento->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($descuento->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->format($descuento->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quantity') ?></th>
            <td><?= $this->Number->format($descuento->quantity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Avalible Amount') ?></th>
            <td><?= $this->Number->format($descuento->avalible_amount) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Producto') ?></h4>
        <?php if (!empty($descuento->producto)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Thumbnail') ?></th>
                <th scope="col"><?= __('Descuento Id') ?></th>
                <th scope="col"><?= __('Categoria Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($descuento->producto as $producto): ?>
            <tr>
                <td><?= h($producto->id) ?></td>
                <td><?= h($producto->name) ?></td>
                <td><?= h($producto->price) ?></td>
                <td><?= h($producto->thumbnail) ?></td>
                <td><?= h($producto->descuento_id) ?></td>
                <td><?= h($producto->categoria_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Producto', 'action' => 'view', $producto->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Producto', 'action' => 'edit', $producto->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Producto', 'action' => 'delete', $producto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $producto->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Venta') ?></h4>
        <?php if (!empty($descuento->venta)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Total Amount') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Is Nulled') ?></th>
                <th scope="col"><?= __('Is Complain') ?></th>
                <th scope="col"><?= __('Complain Detail') ?></th>
                <th scope="col"><?= __('Null Detail') ?></th>
                <th scope="col"><?= __('Caja Id') ?></th>
                <th scope="col"><?= __('Descuento Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($descuento->venta as $venta): ?>
            <tr>
                <td><?= h($venta->id) ?></td>
                <td><?= h($venta->total_price) ?></td>
                <td><?= h($venta->date) ?></td>
                <td><?= h($venta->type) ?></td>
                <td><?= h($venta->is_nulled) ?></td>
                <td><?= h($venta->is_complain) ?></td>
                <td><?= h($venta->complain_detail) ?></td>
                <td><?= h($venta->null_detail) ?></td>
                <td><?= h($venta->caja_id) ?></td>
                <td><?= h($venta->descuento_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Venta', 'action' => 'view', $venta->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Venta', 'action' => 'edit', $venta->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Venta', 'action' => 'delete', $venta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $venta->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
