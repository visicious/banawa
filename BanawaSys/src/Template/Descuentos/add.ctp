<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Descuento $descuento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Descuentos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Producto'), ['controller' => 'Producto', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Producto', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Venta'), ['controller' => 'Venta', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Venta'), ['controller' => 'Venta', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="descuentos form large-9 medium-8 columns content">
    <?= $this->Form->create($descuento) ?>
    <fieldset>
        <legend><?= __('Add Descuento') ?></legend>
        <?php
            echo $this->Form->control('type');
            echo $this->Form->control('price');
            echo $this->Form->control('quantity');
            echo $this->Form->control('avalible_amount');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
