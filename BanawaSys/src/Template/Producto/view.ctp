<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Producto $producto
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Producto'), ['action' => 'edit', $producto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Producto'), ['action' => 'delete', $producto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $producto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Producto'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Producto'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Descuentos'), ['controller' => 'Descuentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Descuento'), ['controller' => 'Descuentos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Categoria'), ['controller' => 'Categoria', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Categorium'), ['controller' => 'Categoria', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Receta'), ['controller' => 'Receta', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recetum'), ['controller' => 'Receta', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Venta'), ['controller' => 'Venta', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Venta'), ['controller' => 'Venta', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="producto view large-9 medium-8 columns content">
    <h3><?= h($producto->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($producto->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Thumbnail') ?></th>
            <td><?= h($producto->thumbnail) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descuento') ?></th>
            <td><?= $producto->has('descuento') ? $this->Html->link($producto->descuento->id, ['controller' => 'Descuentos', 'action' => 'view', $producto->descuento->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Categorium') ?></th>
            <td><?= $producto->has('categorium') ? $this->Html->link($producto->categorium->name, ['controller' => 'Categoria', 'action' => 'view', $producto->categorium->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($producto->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->format($producto->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('¿Está Activo?') ?></th>
            <td><?= $producto->active ? __('Si') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Venta') ?></h4>
        <?php if (!empty($producto->venta)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Total Amount') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Is Nulled') ?></th>
                <th scope="col"><?= __('Is Complain') ?></th>
                <th scope="col"><?= __('Complain Detail') ?></th>
                <th scope="col"><?= __('Null Detail') ?></th>
                <th scope="col"><?= __('Caja Id') ?></th>
                <th scope="col"><?= __('Descuento Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($producto->venta as $venta): ?>
            <tr>
                <td><?= h($venta->id) ?></td>
                <td><?= h($venta->total_price) ?></td>
                <td><?= h($venta->date) ?></td>
                <td><?= h($venta->type) ?></td>
                <td><?= h($venta->is_nulled) ?></td>
                <td><?= h($venta->is_complain) ?></td>
                <td><?= h($venta->complain_detail) ?></td>
                <td><?= h($venta->null_detail) ?></td>
                <td><?= h($venta->caja_id) ?></td>
                <td><?= h($venta->descuento_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Venta', 'action' => 'view', $venta->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Venta', 'action' => 'edit', $venta->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Venta', 'action' => 'delete', $venta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $venta->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Receta') ?></h4>
        <?php if (!empty($producto->receta)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Measure Unity') ?></th>
                <th scope="col"><?= __('Insumo Id') ?></th>
                <th scope="col"><?= __('Producto Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($producto->receta as $receta): ?>
            <tr>
                <td><?= h($receta->id) ?></td>
                <td><?= h($receta->quantity) ?></td>
                <td><?= h($receta->measure_unity) ?></td>
                <td><?= h($receta->insumo_id) ?></td>
                <td><?= h($receta->producto_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Receta', 'action' => 'view', $receta->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Receta', 'action' => 'edit', $receta->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Receta', 'action' => 'delete', $receta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receta->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
