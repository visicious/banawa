<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Venta $venta
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Venta'), ['action' => 'edit', $venta->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Venta'), ['action' => 'delete', $venta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $venta->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Venta'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Venta'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Caja'), ['controller' => 'Caja', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Caja'), ['controller' => 'Caja', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Descuentos'), ['controller' => 'Descuentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Descuento'), ['controller' => 'Descuentos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Producto'), ['controller' => 'Producto', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Producto', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="venta view large-9 medium-8 columns content">
    <h3><?= h($venta->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Tipo de Documento Emitido') ?></th>
            <td><?= h($venta->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Caja') ?></th>
            <td><?= $venta->has('caja') ? $this->Html->link($venta->caja->name, ['controller' => 'Caja', 'action' => 'view', $venta->caja->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descuento') ?></th>
            <td><?= $venta->has('descuento') ? $this->Html->link($venta->descuento->id, ['controller' => 'Descuentos', 'action' => 'view', $venta->descuento->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($venta->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Precio Total') ?></th>
            <td><?= $this->Number->format($venta->total_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha') ?></th>
            <td><?= h($venta->date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Nulled') ?></th>
            <td><?= $venta->is_nulled ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Complain') ?></th>
            <td><?= $venta->is_complain ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Factura RUC') ?></th>
            <td><?= h($venta->factura_RUC) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Factura Razon Social') ?></th>
            <td><?= h($venta->factura_razon_social) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombre de la Orden') ?></th>
            <td><?= h($venta->order_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Monto Pagado') ?></th>
            <td><?= h($venta->pay_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Vuelto') ?></th>
            <td><?= h($venta->pay_change) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Correlativo') ?></th>
            <td><?= h($venta->cod_documento) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Complain Detail') ?></h4>
        <?= $this->Text->autoParagraph(h($venta->complain_detail)); ?>
    </div>
    <div class="row">
        <h4><?= __('Null Detail') ?></h4>
        <?= $this->Text->autoParagraph(h($venta->null_detail)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Producto') ?></h4>
        <?php if (!empty($venta->producto)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Thumbnail') ?></th>
                <th scope="col"><?= __('Descuento Id') ?></th>
                <th scope="col"><?= __('Categoria Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($venta->producto as $producto): ?>
            <tr>
                <td><?= h($producto->id) ?></td>
                <td><?= h($producto->name) ?></td>
                <td><?= h($producto->price) ?></td>
                <td><?= h($producto->thumbnail) ?></td>
                <td><?= h($producto->descuento_id) ?></td>
                <td><?= h($producto->categoria_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Producto', 'action' => 'view', $producto->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Producto', 'action' => 'edit', $producto->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Producto', 'action' => 'delete', $producto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $producto->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
