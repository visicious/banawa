<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Venta[]|\Cake\Collection\CollectionInterface $venta
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Venta'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Caja'), ['controller' => 'Caja', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Caja'), ['controller' => 'Caja', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Descuentos'), ['controller' => 'Descuentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Descuento'), ['controller' => 'Descuentos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Producto'), ['controller' => 'Producto', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Producto', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="venta index large-9 medium-8 columns content">
    <h3><?= __('Venta') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_nulled') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_complain') ?></th>
                <th scope="col"><?= $this->Paginator->sort('factura_RUC')  ?></th>
                <th scope="col"><?= $this->Paginator->sort('factura_razon_social') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pay_amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pay_change') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cod_documento')?></th>
                <th scope="col"><?= $this->Paginator->sort('caja_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('descuento_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($venta as $venta): ?>
            <tr>
                <td><?= $this->Number->format($venta->id) ?></td>
                <td><?= $this->Number->format($venta->total_price) ?></td>
                <td><?= h($venta->date) ?></td>
                <td><?= h($venta->type) ?></td>
                <td><?= h($venta->is_nulled) ?></td>
                <td><?= h($venta->is_complain) ?></td>
                <td><?= h($venta->factura_RUC) ?></td>
                <td><?= h($venta->factura_razon_social) ?></td>
                <td><?= h($venta->order_name) ?></td>
                <td><?= h($venta->pay_amount) ?></td>
                <td><?= h($venta->pay_change) ?></td>
                <td><?= h($venta->cod_documento) ?></td>
                <td><?= $venta->has('caja') ? $this->Html->link($venta->caja->name, ['controller' => 'Caja', 'action' => 'view', $venta->caja->id]) : '' ?></td>
                <td><?= $venta->has('descuento') ? $this->Html->link($venta->descuento->id, ['controller' => 'Descuentos', 'action' => 'view', $venta->descuento->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $venta->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $venta->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $venta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $venta->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
