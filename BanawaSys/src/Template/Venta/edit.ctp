<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Venta $venta
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $venta->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $venta->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Venta'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Caja'), ['controller' => 'Caja', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Caja'), ['controller' => 'Caja', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Descuentos'), ['controller' => 'Descuentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Descuento'), ['controller' => 'Descuentos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Producto'), ['controller' => 'Producto', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Producto', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="venta form large-9 medium-8 columns content">
    <?= $this->Form->create($venta) ?>
    <fieldset>
        <legend><?= __('Edit Venta') ?></legend>
        <?php
            echo $this->Form->control('total_price');
            echo $this->Form->control('date');
            echo $this->Form->control('type');
            echo $this->Form->control('is_nulled');
            echo $this->Form->control('is_complain');
            echo $this->Form->control('complain_detail');
            echo $this->Form->control('null_detail');
            echo $this->Form->control('factura_RUC');
            echo $this->Form->control('factura_razon_social');
            echo $this->Form->control('order_name');
            echo $this->Form->control('pay_amount');
            echo $this->Form->control('pay_change');
            echo $this->Form->control('cod_documento');
            echo $this->Form->control('caja_id', ['options' => $caja]);
            echo $this->Form->control('descuento_id', ['options' => $descuentos, 'empty' => true]);
            echo $this->Form->control('producto._ids', ['options' => $producto]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
