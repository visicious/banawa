<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Recetum $recetum
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Recetum'), ['action' => 'edit', $recetum->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Recetum'), ['action' => 'delete', $recetum->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recetum->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Receta'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recetum'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Insumo'), ['controller' => 'Insumo', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Insumo'), ['controller' => 'Insumo', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Producto'), ['controller' => 'Producto', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Producto', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="receta view large-9 medium-8 columns content">
    <h3><?= h($recetum->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Measure Unity') ?></th>
            <td><?= h($recetum->measure_unity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Insumo') ?></th>
            <td><?= $recetum->has('insumo') ? $this->Html->link($recetum->insumo->name, ['controller' => 'Insumo', 'action' => 'view', $recetum->insumo->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Producto') ?></th>
            <td><?= $recetum->has('producto') ? $this->Html->link($recetum->producto->name, ['controller' => 'Producto', 'action' => 'view', $recetum->producto->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($recetum->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quantity') ?></th>
            <td><?= $this->Number->format($recetum->quantity) ?></td>
        </tr>
    </table>
</div>
