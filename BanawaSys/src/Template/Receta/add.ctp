<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Recetum $recetum
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Receta'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Insumo'), ['controller' => 'Insumo', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Insumo'), ['controller' => 'Insumo', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Producto'), ['controller' => 'Producto', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Producto', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receta form large-9 medium-8 columns content">
    <?= $this->Form->create($recetum) ?>
    <fieldset>
        <legend><?= __('Add Recetum') ?></legend>
        <?php
            echo $this->Form->control('quantity');
            echo $this->Form->control('measure_unity');
            echo $this->Form->control('insumo_id', ['options' => $insumo]);
            echo $this->Form->control('producto_id', ['options' => $producto]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
