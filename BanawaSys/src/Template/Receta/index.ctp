<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Recetum[]|\Cake\Collection\CollectionInterface $receta
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Recetum'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Insumo'), ['controller' => 'Insumo', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Insumo'), ['controller' => 'Insumo', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Producto'), ['controller' => 'Producto', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Producto', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receta index large-9 medium-8 columns content">
    <h3><?= __('Receta') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('quantity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('measure_unity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('insumo_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('producto_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($receta as $recetum): ?>
            <tr>
                <td><?= $this->Number->format($recetum->id) ?></td>
                <td><?= $this->Number->format($recetum->quantity) ?></td>
                <td><?= h($recetum->measure_unity) ?></td>
                <td><?= $recetum->has('insumo') ? $this->Html->link($recetum->insumo->name, ['controller' => 'Insumo', 'action' => 'view', $recetum->insumo->id]) : '' ?></td>
                <td><?= $recetum->has('producto') ? $this->Html->link($recetum->producto->name, ['controller' => 'Producto', 'action' => 'view', $recetum->producto->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $recetum->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $recetum->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $recetum->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recetum->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
