<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Empleado $empleado
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $empleado->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $empleado->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Empleado'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Caja'), ['controller' => 'Caja', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Caja'), ['controller' => 'Caja', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="empleado form large-9 medium-8 columns content">
    <?= $this->Form->create($empleado) ?>
    <fieldset>
        <legend><?= __('Edit Empleado') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('failures');
            echo $this->Form->control('payment');
            echo $this->Form->control('hire_date');
            echo $this->Form->control('caja._ids', ['options' => $caja]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
