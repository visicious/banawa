<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Empleado $empleado
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Empleado'), ['action' => 'edit', $empleado->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Empleado'), ['action' => 'delete', $empleado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $empleado->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Empleado'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Empleado'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Caja'), ['controller' => 'Caja', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Caja'), ['controller' => 'Caja', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="empleado view large-9 medium-8 columns content">
    <h3><?= h($empleado->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($empleado->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($empleado->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Failures') ?></th>
            <td><?= $this->Number->format($empleado->failures) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Payment') ?></th>
            <td><?= $this->Number->format($empleado->payment) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hire Date') ?></th>
            <td><?= h($empleado->hire_date) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Caja') ?></h4>
        <?php if (!empty($empleado->caja)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('User') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Original Password') ?></th>
                <th scope="col"><?= __('Location') ?></th>
                <th scope="col"><?= __('Workers Amount') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($empleado->caja as $caja): ?>
            <tr>
                <td><?= h($caja->id) ?></td>
                <td><?= h($caja->name) ?></td>
                <td><?= h($caja->type) ?></td>
                <td><?= h($caja->user) ?></td>
                <td><?= h($caja->password) ?></td>
                <td><?= h($caja->original_password) ?></td>
                <td><?= h($caja->location) ?></td>
                <td><?= h($caja->workers_amount) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Caja', 'action' => 'view', $caja->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Caja', 'action' => 'edit', $caja->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Caja', 'action' => 'delete', $caja->id], ['confirm' => __('Are you sure you want to delete # {0}?', $caja->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
