<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Empleado[]|\Cake\Collection\CollectionInterface $empleado
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Empleado'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Caja'), ['controller' => 'Caja', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Caja'), ['controller' => 'Caja', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="empleado index large-9 medium-8 columns content">
    <h3><?= __('Empleado') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('failures') ?></th>
                <th scope="col"><?= $this->Paginator->sort('payment') ?></th>
                <th scope="col"><?= $this->Paginator->sort('hire_date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($empleado as $empleado): ?>
            <tr>
                <td><?= $this->Number->format($empleado->id) ?></td>
                <td><?= h($empleado->name) ?></td>
                <td><?= $this->Number->format($empleado->failures) ?></td>
                <td><?= $this->Number->format($empleado->payment) ?></td>
                <td><?= h($empleado->hire_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $empleado->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $empleado->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $empleado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $empleado->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
