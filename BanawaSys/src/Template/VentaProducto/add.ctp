<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\VentaProducto $ventaProducto
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Venta Producto'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Producto'), ['controller' => 'Producto', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Producto', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Venta'), ['controller' => 'Venta', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Venta'), ['controller' => 'Venta', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ventaProducto form large-9 medium-8 columns content">
    <?= $this->Form->create($ventaProducto) ?>
    <fieldset>
        <legend><?= __('Add Venta Producto') ?></legend>
        <?php
            echo $this->Form->control('amount');
            echo $this->Form->control('producto_id', ['options' => $producto]);
            echo $this->Form->control('venta_id', ['options' => $venta]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
