<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\VentaProducto $ventaProducto
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Venta Producto'), ['action' => 'edit', $ventaProducto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Venta Producto'), ['action' => 'delete', $ventaProducto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ventaProducto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Venta Producto'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Venta Producto'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Producto'), ['controller' => 'Producto', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Producto', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Venta'), ['controller' => 'Venta', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Venta'), ['controller' => 'Venta', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ventaProducto view large-9 medium-8 columns content">
    <h3><?= h($ventaProducto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Producto') ?></th>
            <td><?= $ventaProducto->has('producto') ? $this->Html->link($ventaProducto->producto->name, ['controller' => 'Producto', 'action' => 'view', $ventaProducto->producto->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Venta') ?></th>
            <td><?= $ventaProducto->has('venta') ? $this->Html->link($ventaProducto->venta->id, ['controller' => 'Venta', 'action' => 'view', $ventaProducto->venta->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ventaProducto->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->format($ventaProducto->amount) ?></td>
        </tr>
    </table>
</div>
