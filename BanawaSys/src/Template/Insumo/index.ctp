<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Insumo[]|\Cake\Collection\CollectionInterface $insumo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Insumo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Receta'), ['controller' => 'Receta', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recetum'), ['controller' => 'Receta', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="insumo index large-9 medium-8 columns content">
    <h3><?= __('Insumo') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('quantity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('measure_unity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('buy_amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('buy_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('provider') ?></th>
                <th scope="col"><?= $this->Paginator->sort('waste') ?></th>
                <th scope="col"><?= $this->Paginator->sort('avalible_items') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($insumo as $insumo): ?>
            <tr>
                <td><?= $this->Number->format($insumo->id) ?></td>
                <td><?= $this->Number->format($insumo->quantity) ?></td>
                <td><?= h($insumo->measure_unity) ?></td>
                <td><?= $this->Number->format($insumo->buy_amount) ?></td>
                <td><?= h($insumo->name) ?></td>
                <td><?= $this->Number->format($insumo->price) ?></td>
                <td><?= h($insumo->buy_date) ?></td>
                <td><?= h($insumo->provider) ?></td>
                <td><?= $this->Number->format($insumo->waste) ?></td>
                <td><?= $this->Number->format($insumo->avalible_items) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $insumo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $insumo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $insumo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $insumo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
