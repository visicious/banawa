<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Insumo $insumo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Insumo'), ['action' => 'edit', $insumo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Insumo'), ['action' => 'delete', $insumo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $insumo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Insumo'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Insumo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Receta'), ['controller' => 'Receta', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recetum'), ['controller' => 'Receta', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="insumo view large-9 medium-8 columns content">
    <h3><?= h($insumo->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Measure Unity') ?></th>
            <td><?= h($insumo->measure_unity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($insumo->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Provider') ?></th>
            <td><?= h($insumo->provider) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($insumo->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quantity') ?></th>
            <td><?= $this->Number->format($insumo->quantity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Buy Amount') ?></th>
            <td><?= $this->Number->format($insumo->buy_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->format($insumo->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Waste') ?></th>
            <td><?= $this->Number->format($insumo->waste) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Avalible Items') ?></th>
            <td><?= $this->Number->format($insumo->avalible_items) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Buy Date') ?></th>
            <td><?= h($insumo->buy_date) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Receta') ?></h4>
        <?php if (!empty($insumo->receta)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Measure Unity') ?></th>
                <th scope="col"><?= __('Insumo Id') ?></th>
                <th scope="col"><?= __('Producto Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($insumo->receta as $receta): ?>
            <tr>
                <td><?= h($receta->id) ?></td>
                <td><?= h($receta->quantity) ?></td>
                <td><?= h($receta->measure_unity) ?></td>
                <td><?= h($receta->insumo_id) ?></td>
                <td><?= h($receta->producto_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Receta', 'action' => 'view', $receta->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Receta', 'action' => 'edit', $receta->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Receta', 'action' => 'delete', $receta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receta->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
