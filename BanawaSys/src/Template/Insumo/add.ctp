<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Insumo $insumo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Insumo'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Receta'), ['controller' => 'Receta', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recetum'), ['controller' => 'Receta', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="insumo form large-9 medium-8 columns content">
    <?= $this->Form->create($insumo) ?>
    <fieldset>
        <legend><?= __('Add Insumo') ?></legend>
        <?php
            echo $this->Form->control('quantity');
            echo $this->Form->control('measure_unity');
            echo $this->Form->control('buy_amount');
            echo $this->Form->control('name');
            echo $this->Form->control('price');
            echo $this->Form->control('buy_date');
            echo $this->Form->control('provider');
            echo $this->Form->control('waste');
            echo $this->Form->control('avalible_items');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
