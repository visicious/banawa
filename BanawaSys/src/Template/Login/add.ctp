<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Caja $caja
 */
?>
<div class="caja form large-9 medium-8 columns content">
    <?= $this->Form->create($caja) ?>
    <fieldset>
        <legend><?= __('Agregar Usuario (Caja)') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('type');
            echo $this->Form->control('user');
            echo $this->Form->control('original_password');
            echo $this->Form->control('location');
            echo $this->Form->control('workers_amount');
            echo $this->Form->control('empleado._ids', ['options' => $empleado]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Agregar')) ?>
    <?= $this->Form->end() ?>
</div>
