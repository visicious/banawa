<?php
    use Cake\Core\App;
    //including the FusionCharts PHP wrapper
    require_once(ROOT .DS. 'vendor' . DS  . 'fusioncharts' . DS . 'fusioncharts.php');
    $arrData = array(
        "chart" => array(
            "animation"=>"0",
            "caption"=> "Banawa",
            "subCaption"=> "Cantidad de Productos Comprados",
            "xAxisName"=> "Producto",
            "yAxisName"=> "Cantidad",
            "showValues"=> "1",
            "paletteColors"=> "#81BB76", 
            "showHoverEffect"=> "1",
            "use3DLighting"=> "0",
            "showaxislines"=> "1",
            "baseFontSize"=> "13",
            "theme"=> "fint",
            "labeldisplay" => "rotate",
            "useellipseswhenoverflow"=> "1"
        )
    );
    $data = [];
    foreach($query as $row) {
        $value = $row["amount"];
        array_push($data, array(
            "label" => $row["producto"]["name"], 
            "value" => $row["amount"]) 
        );
    }
    sort($data);

    $new_data = [];
    foreach($data as $index => $row) {
        if ($index == 0 ){
            array_push($new_data, array(
                "label" => $row["label"], 
                "value" => $row["value"]) 
            );
        } else {
            if ($data[$index-1]["label"] == $row["label"]){
                $new_data[count($new_data)-1]["value"] += $row["value"];
            } else {
                array_push($new_data, array(
                    "label" => $row["label"], 
                    "value" => $row["value"])
                );
            }
        }
    }

    $arrData["data"]=$new_data;

    //encoding the dataset object in json format
    $jsonEncodedData = json_encode($arrData);

    //print_r($arrData);
    // chart object
    $columnChart = new FusionCharts("column2d", "chart-1", "100%", "800", "chart-container", "json", $jsonEncodedData);
 
    // Render the chart
    $columnChart->render();
?>

<div id="chart-container"></div>