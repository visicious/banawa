<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Caja[]|\Cake\Collection\CollectionInterface $caja
 large-9 medium-8
 */
?>

<div class="banawa-container clearfix" data=<?= $actual_caja["id"] ?> >

    <nav class="banawa-order large-3 medium-4 small-4 columns ">
        <div class="banawa-order-list"></div>
        <button type="button" class="banawa-button banawa-complete-sale large" onclick="renderSaleTypes();">Generar Comprobante</button>
        <div class="banawa-price-all-button">S/. 0.00</div>
    </nav>
    <div class="banawa-home large-9 medium-8 small-8 index columns">
        <div class="banawa-overlay banawa-quantity-container container" >
            <div class="banawa-options-input row ">
                <div class="col-md-11 col-11 "></div>
                <button type="button" class="banawa-close-button col-md-1 col-1 btn btn-red"  onclick="closeOverlay();"> X </button>
                <div class="col-md-6 col-sm-6 col-12">
                    <div class="banawa-option-input row align-items-center">
                        
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-12">
                    <div class="banawa-quantity-input row align-items-center">
                        <div class="col-md-12 col-12 banawa-info">¿CUANTOS ITEMS DESEA EL CLIENTE?</div>
                        <div class="clearfix"><br/></div>
                        <input type="number" name="item-quantity" class="col-md-12 col-12 " />
                        <button type="button" class="banawa-button banawa-quantity-number medium col-md-4 col-sm-4 col-4">1</button>
                        <button type="button" class="banawa-button banawa-quantity-number medium col-md-4 col-sm-4 col-4">2</button>
                        <button type="button" class="banawa-button banawa-quantity-number medium col-md-4 col-sm-4 col-4">3</button>
                        <button type="button" class="banawa-button banawa-quantity-number medium col-md-4 col-sm-4 col-4">4</button>
                        <button type="button" class="banawa-button banawa-quantity-number medium col-md-4 col-sm-4 col-4">5</button>
                        <button type="button" class="banawa-button banawa-quantity-number medium col-md-4 col-sm-4 col-4">6</button>
                        <button type="button" class="banawa-button banawa-quantity-number medium col-md-4 col-sm-4 col-4">7</button>
                        <button type="button" class="banawa-button banawa-quantity-number medium col-md-4 col-sm-4 col-4">8</button>
                        <button type="button" class="banawa-button banawa-quantity-number medium col-md-4 col-sm-4 col-4">9</button>
                        <button type="button" class="banawa-button medium col-md-4 col-sm-4 col-4" onclick="deleteLastInputVal();">Borrar</button>
                        <button type="button" class="banawa-button banawa-quantity-number medium col-md-4 col-sm-4 col-4">0</button>
                        <button type="button" class="banawa-button banawa-continue medium col-md-4 col-sm-4 col-4" onclick="modifyProductQuantity();">Continuar</button>
                    </div>
                </div>
                
            </div>
        </div>
        <button type="button" class="banawa-backwards" onclick="renderCategories();"><-</button>
        <div class="banawa-categorias row">
            <?php foreach ($available_categories as $category): ?>
            <div class="categoria banawa-button col-md-3 col-sm-4 col-4 banawa-background-img" data=<?= $category->id ?>>
                <a>
                    <img src=<?= $category->thumbnail ?> />
                    <div class="banawa-title">
                        <?= $category->name ?>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="banawa-overlay banawa-sale-type-container container" >
        <div class="banawa-sale-type row align-items-center">
            <input type="text" class="banawa-input-razon-social col-md-5 col-sm-5 col-12" style="display: none" name="razon-social" placeholder="RAZON SOCIAL" class="col-md-12" />
            <input type="number" class="banawa-input-ruc col-md-5 col-sm-5 col-12"  style="display: none" name="RUC" placeholder="RUC" class="col-md-12" pattern="[0-9]{11}" />
            <button type="button" class="banawa-boleta banawa-button medium col-md-5 col-sm-5 col-5" data-type="boleta" onclick="setSaleType();">Boleta</button>
            <button type="button" class="banawa-factura banawa-button medium col-md-5 col-sm-5 col-5" data-type="factura" onclick="setSaleType();">Factura</button>
        </div>
    </div>

</div>
 