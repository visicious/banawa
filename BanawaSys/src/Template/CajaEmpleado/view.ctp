<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CajaEmpleado $cajaEmpleado
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Caja Empleado'), ['action' => 'edit', $cajaEmpleado->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Caja Empleado'), ['action' => 'delete', $cajaEmpleado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cajaEmpleado->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Caja Empleado'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Caja Empleado'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Empleado'), ['controller' => 'Empleado', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Empleado'), ['controller' => 'Empleado', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Caja'), ['controller' => 'Caja', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Caja'), ['controller' => 'Caja', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cajaEmpleado view large-9 medium-8 columns content">
    <h3><?= h($cajaEmpleado->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Empleado') ?></th>
            <td><?= $cajaEmpleado->has('empleado') ? $this->Html->link($cajaEmpleado->empleado->name, ['controller' => 'Empleado', 'action' => 'view', $cajaEmpleado->empleado->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Caja') ?></th>
            <td><?= $cajaEmpleado->has('caja') ? $this->Html->link($cajaEmpleado->caja->name, ['controller' => 'Caja', 'action' => 'view', $cajaEmpleado->caja->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($cajaEmpleado->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hire Date') ?></th>
            <td><?= h($cajaEmpleado->hire_date) ?></td>
        </tr>
    </table>
</div>
