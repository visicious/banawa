<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CajaEmpleado $cajaEmpleado
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $cajaEmpleado->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $cajaEmpleado->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Caja Empleado'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Empleado'), ['controller' => 'Empleado', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Empleado'), ['controller' => 'Empleado', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Caja'), ['controller' => 'Caja', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Caja'), ['controller' => 'Caja', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cajaEmpleado form large-9 medium-8 columns content">
    <?= $this->Form->create($cajaEmpleado) ?>
    <fieldset>
        <legend><?= __('Edit Caja Empleado') ?></legend>
        <?php
            echo $this->Form->control('hire_date');
            echo $this->Form->control('empleado_id', ['options' => $empleado]);
            echo $this->Form->control('caja_id', ['options' => $caja]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
