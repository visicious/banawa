<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CajaEmpleado[]|\Cake\Collection\CollectionInterface $cajaEmpleado
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Caja Empleado'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Empleado'), ['controller' => 'Empleado', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Empleado'), ['controller' => 'Empleado', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Caja'), ['controller' => 'Caja', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Caja'), ['controller' => 'Caja', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cajaEmpleado index large-9 medium-8 columns content">
    <h3><?= __('Caja Empleado') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('hire_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('empleado_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('caja_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cajaEmpleado as $cajaEmpleado): ?>
            <tr>
                <td><?= $this->Number->format($cajaEmpleado->id) ?></td>
                <td><?= h($cajaEmpleado->hire_date) ?></td>
                <td><?= $cajaEmpleado->has('empleado') ? $this->Html->link($cajaEmpleado->empleado->name, ['controller' => 'Empleado', 'action' => 'view', $cajaEmpleado->empleado->id]) : '' ?></td>
                <td><?= $cajaEmpleado->has('caja') ? $this->Html->link($cajaEmpleado->caja->name, ['controller' => 'Caja', 'action' => 'view', $cajaEmpleado->caja->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $cajaEmpleado->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $cajaEmpleado->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $cajaEmpleado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cajaEmpleado->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
