<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Caja $caja
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $caja->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $caja->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Caja'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Venta'), ['controller' => 'Venta', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Venta'), ['controller' => 'Venta', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Empleado'), ['controller' => 'Empleado', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Empleado'), ['controller' => 'Empleado', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="caja form large-9 medium-8 columns content">
    <?= $this->Form->create($caja) ?>
    <fieldset>
        <legend><?= __('Edit Caja') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('type');
            echo $this->Form->control('user');
            echo $this->Form->control('original_password');
            echo $this->Form->control('location');
            echo $this->Form->control('workers_amount');
            echo $this->Form->control('empleado._ids', ['options' => $empleado]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
