<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Caja $caja
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Caja'), ['action' => 'edit', $caja->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Caja'), ['action' => 'delete', $caja->id], ['confirm' => __('Are you sure you want to delete # {0}?', $caja->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Caja'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Caja'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Venta'), ['controller' => 'Venta', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Venta'), ['controller' => 'Venta', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Empleado'), ['controller' => 'Empleado', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Empleado'), ['controller' => 'Empleado', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="caja view large-9 medium-8 columns content">
    <h3><?= h($caja->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($caja->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($caja->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= h($caja->user) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Original Password') ?></th>
            <td><?= h($caja->original_password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Location') ?></th>
            <td><?= h($caja->location) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($caja->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Workers Amount') ?></th>
            <td><?= $this->Number->format($caja->workers_amount) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Empleado') ?></h4>
        <?php if (!empty($caja->empleado)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Failures') ?></th>
                <th scope="col"><?= __('Payment') ?></th>
                <th scope="col"><?= __('Hire Date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($caja->empleado as $empleado): ?>
            <tr>
                <td><?= h($empleado->id) ?></td>
                <td><?= h($empleado->name) ?></td>
                <td><?= h($empleado->failures) ?></td>
                <td><?= h($empleado->payment) ?></td>
                <td><?= h($empleado->hire_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Empleado', 'action' => 'view', $empleado->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Empleado', 'action' => 'edit', $empleado->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Empleado', 'action' => 'delete', $empleado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $empleado->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Venta') ?></h4>
        <?php if (!empty($caja->venta)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Total Amount') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Is Nulled') ?></th>
                <th scope="col"><?= __('Is Complain') ?></th>
                <th scope="col"><?= __('Complain Detail') ?></th>
                <th scope="col"><?= __('Null Detail') ?></th>
                <th scope="col"><?= __('Caja Id') ?></th>
                <th scope="col"><?= __('Descuento Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($caja->venta as $venta): ?>
            <tr>
                <td><?= h($venta->id) ?></td>
                <td><?= h($venta->total_price) ?></td>
                <td><?= h($venta->date) ?></td>
                <td><?= h($venta->type) ?></td>
                <td><?= h($venta->is_nulled) ?></td>
                <td><?= h($venta->is_complain) ?></td>
                <td><?= h($venta->complain_detail) ?></td>
                <td><?= h($venta->null_detail) ?></td>
                <td><?= h($venta->caja_id) ?></td>
                <td><?= h($venta->descuento_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Venta', 'action' => 'view', $venta->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Venta', 'action' => 'edit', $venta->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Venta', 'action' => 'delete', $venta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $venta->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
