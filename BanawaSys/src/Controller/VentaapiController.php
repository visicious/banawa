<?php
namespace App\Controller;
require 'C:\banawa\BanawaSys\vendor\mike42\escpos-php\autoload.php';
require ROOT .DS.'\src\Libraries\Util.php';

use App\Controller\AppapiController;
use Cake\ORM\TableRegistry;

use Cake\I18n\Time;
use Cake\I18n\Number;

use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;

use Greenter\Model\Client\Client;
use Greenter\Model\Sale\Invoice;
use Greenter\Model\Sale\SaleDetail;
use Greenter\Model\Sale\Legend;
use Greenter\Ws\Services\SunatEndpoints;
use Greenter\Model\Company\Address;
use Greenter\Model\Company\Company;
use Util;

class VentaapiController extends AppapiController
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->table_venta = TableRegistry::get('Venta');
        $this->table_venta_producto = TableRegistry::get('VentaProducto');
        $this->table_producto = TableRegistry::get('Producto');
        $this->caja = 1; //Cambiar cuando esten los permisos y el login
    }

    
    private function serie_formater($nro_serie){
        if(strlen($nro_serie) == 1){
            return "00".$nro_serie;
        } else if(strlen($nro_serie) == 2){
            return "0".$nro_serie;
        }
        return $nro_serie;
    }

    private function get_cod_document($data) {
        $last_venta = $this->table_venta->find("all")
            ->where(['Venta.type =' => $data["type"]])
            ->all()
            ->last();


        if(empty($last_venta) && $data["type"] == "boleta") {
            //Generar el primer codigo para boleta
            return "B001-1";
        } else if(empty($last_venta) && $data["type"] == "factura") {
            //Generar el primer codigo para factura
            return "F001-1";
        } else if($data["type"] == "nota_venta") {
            //Generar el primer codigo para factura
            return "NV";
        } else {
            $correlativo = explode("-",$last_venta["cod_documento"]);
            if (strlen($correlativo[1]) > 8) {
                $serie = "";
                if ($data["type"] == "factura") {
                    $serie = explode("F",$correlativo[0]);
                    $serie = "F".$this->serie_formater(intval($serie[0]) + 1);
                } else if ($data["type"] == "boleta") {
                    $serie = explode("B",$correlativo[0]);
                    $serie = "B".$this->serie_formater(intval($serie[0]) + 1);
                }

                return $serie."-1";
            } else {
                return $correlativo[0]."-".(intval($correlativo[1]) + 1);
            }
        }
    }

    public function add()
    {
        $income_data_venta = $this->request->getData();
        $income_data_venta["is_nulled"] = 0;
        $income_data_venta["is_complain"] = 0;
        $income_data_venta["null_detail"] = "";
        $income_data_venta["complain_detail"] = "";
        $income_data_venta["date"]["second"] = date("s");
        $income_data_venta["cod_documento"] = $this->get_cod_document($income_data_venta);
        $income_data_venta_printer = $income_data_venta;
        // hacer validacion entre el id de la caja y el que llega

        $income_data_venta_productos = $income_data_venta["productos"];
        unset($income_data_venta["productos"]);     

        $venta = $this->table_venta->newEntity();
        if ($this->request->is('post')) {
            $venta = $this->table_venta->patchEntity($venta, $income_data_venta);
            $income_data_venta_printer["date"] = $venta["date"];
            $result = $this->table_venta->save($venta);
            if ($result) {
                //$this->Flash->success(__('Venta realizada con exito.'));

                foreach ($income_data_venta_productos as $prd_id => $amount) {
                    $venta_producto = $this->table_venta_producto->newEntity();
                    $venta_producto->producto_id = $prd_id;
                    $venta_producto->venta_id = $result->id;
                    $venta_producto->amount = $amount;

                    $each_venta = $this->table_venta_producto->save($venta_producto);
                    if ($each_venta) {
                        continue;
                    } else {
                        //$this->Flash->error(__('Hubo un problema con la venta. Contactar al administrador'));
                        $this->set([
                            'message' => $income_data_venta,
                            '_serialize' => ['message']
                        ]);
                    }
                }

            } else {
                //$this->Flash->error(__('Hubo un problema con la venta. Contactar al administrador'));
                $this->set([
                    'message' => $income_data_venta,
                    '_serialize' => ['message']
                ]);
            }
        }        
        $message = [];
        $message["message"] = "Todo bien";//$this->generate_sunat_doc($income_data_venta_printer);

        if ($income_data_venta["type"] == "boleta") {
            $this->print_boleta($income_data_venta_printer);
        } else if ($income_data_venta["type"] == "factura") {
            $this->print_factura($income_data_venta_printer);
        } else if ($income_data_venta["type"] == "nota_venta") {
            $this->print_nota_venta($income_data_venta_printer);
        }
        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
    }

     private function print_nota_venta($data){
        $connector = new FilePrintConnector("//localhost/Star BSC10");
        $printer = new Printer($connector);
        //print_r($income_data_venta_printer);

        
        //Auxiliares para el texto
        date_default_timezone_set('America/Lima');
        $datetime = Time::parse($data["date"]);
        $date = $datetime->i18nFormat('yyyy/MM/dd');
        $time = $datetime->i18nFormat('HH:mm:ss');
        $max_length_prd = 22;
        $spaces_right = 0;

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $Data = "";
        $Data .= "\n";
        $Data .= "BANAWA JUICE BAR S.R.L.\n";
        $Data .= "RUC 20602896103\n";
        $Data .= "Cal.Santa Catalina Nro. 211 (Al Frente De La Alianza Francesa) Arequipa\n";
        $Data .= "Arequipa - Arequipa\n";
        $Data .= "NOTA DE VENTA \n";
        $Data .= $data["cod_documento"]."\n\n";
        $printer->text($Data);

        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $Data = "";
        $Data .= "FECHA DE EMISION: ".$date."\n";
        $Data .= "HORA: ".$time."\n";
        $Data .= "TIPO DE MONEDA: NUEVO SOL\n\n";
        $printer->text($Data);

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $Data = "";
        $Data .= "------------------------------------------\n";
        $Data .= "CODIGO       DESCRIPCION             TOTAL\n";
        $Data .= "     CANT. X PRECIO UNIT.\n";
        $Data .= "------------------------------------------\n";
        $printer->text($Data);

        foreach ($data["productos"] as $prd_id => $amount) {
            $prd = $this->table_producto->get($prd_id);
            $prd_name = strtoupper($prd["name"]);
            if (substr($prd_name, $max_length_prd)) {
                $prd_name = substr($prd_name, 0, $max_length_prd);
            } else {
                $spaces_right = $max_length_prd - strlen($prd_name);
                $prd_name .= str_repeat(" ", $spaces_right);
            }
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text("BNW-00".$prd_id);
            $printer->text("   ".$amount." x ".$prd_name." ".number_format(round($amount*$prd["price"], 2),2)."\n");
        }
        $printer->setJustification(Printer::JUSTIFY_RIGHT);
        $Data = "";
        $Data .= "\nSUBTOTAL              S/.           ".number_format(round($data["total_price"] / 1.18 ,2),2) ."\n\n";
        $Data .= "OP. EXONERADA                        0.00\n";
        $Data .= "OP. INAFECTA                         0.00\n";
        $Data .= "OP. GRAVADA                         ".number_format(round($data["total_price"] / 1.18 ,2),2) ."\n\n";
        $Data .= "I.S.C.              S/.              0.00\n";
        $Data .= "I.G.V.              S/.             ".round($data["total_price"] - ($data["total_price"] / 1.18) ,2) ."\n";
        $Data .= "IMPORTE TOTAL       S/.             ".number_format($data["total_price"],2)."\n";
        $Data .= "TOTAL A PAGAR       S/.             ".number_format($data["total_price"],2)."\n\n";
        $Data .= "VUELTO                              ".number_format($data["pay_change"],2)."\n";
        $printer->text($Data);

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $Data = "";
        $Data .= "ESTE DOCUMENTO NO REPRESENTA UNA BOLETA ELECTRÓNICA SINO EL CONSUMO EN EL LOCAL. PARA REMITIR LA BOLETA CORRESPONDIENTE, SOLICITELA EN CAJA\n";
        $Data .= "******************************************\n";
        $Data .= "¡GRACIAS POR SU COMPRA!\n";
        $Data .= "VUELVA PRONTO\n";


        $printer->text($Data."\n\n\n\n");
        $printer->cut();
        $this->print_comanda($printer,$data["order_name"], $data["productos"]);
        $printer->pulse(0, 120, 240);
        $printer->close();
    }

    private function print_boleta($data){
        $connector = new FilePrintConnector("//localhost/Star BSC10");
        $printer = new Printer($connector);
        //print_r($income_data_venta_printer);

        
        //Auxiliares para el texto
        date_default_timezone_set('America/Lima');
        $datetime = Time::parse($data["date"]);
        $date = $datetime->i18nFormat('yyyy/MM/dd');
        $time = $datetime->i18nFormat('HH:mm:ss');
        $max_length_prd = 22;
        $spaces_right = 0;

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $Data = "";
        $Data .= "\n";
        $Data .= "BANAWA JUICE BAR S.R.L.\n";
        $Data .= "RUC 20602896103\n";
        $Data .= "Cal.Santa Catalina Nro. 211 (Al Frente De La Alianza Francesa) Arequipa\n";
        $Data .= "Arequipa - Arequipa\n";
        $Data .= "BOLETA DE VENTA ELECTRÓNICA\n";
        $Data .= $data["cod_documento"]."\n\n";
        $printer->text($Data);

        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $Data = "";
        $Data .= "CORRELATIVO: ".$data["cod_documento"]."\n";
        $Data .= "FECHA DE EMISION: ".$date."\n";
        $Data .= "HORA: ".$time."\n";
        $Data .= "TIPO DE MONEDA: NUEVO SOL\n\n";
        $printer->text($Data);

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $Data = "";
        $Data .= "------------------------------------------\n";
        $Data .= "CODIGO       DESCRIPCION             TOTAL\n";
        $Data .= "     CANT. X PRECIO UNIT.\n";
        $Data .= "------------------------------------------\n";
        $printer->text($Data);

        foreach ($data["productos"] as $prd_id => $amount) {
            $prd = $this->table_producto->get($prd_id);
            $prd_name = strtoupper($prd["name"]);
            if (substr($prd_name, $max_length_prd)) {
                $prd_name = substr($prd_name, 0, $max_length_prd);
            } else {
                $spaces_right = $max_length_prd - strlen($prd_name);
                $prd_name .= str_repeat(" ", $spaces_right);
            }
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text("BNW-00".$prd_id);
            $printer->text("   ".$amount." x ".$prd_name." ".number_format(round($amount*$prd["price"], 2),2)."\n");
        }
        $printer->setJustification(Printer::JUSTIFY_RIGHT);
        $Data = "";
        $Data .= "\nSUBTOTAL              S/.           ".number_format(round($data["total_price"] / 1.18 ,2),2) ."\n\n";
        $Data .= "OP. EXONERADA                        0.00\n";
        $Data .= "OP. INAFECTA                         0.00\n";
        $Data .= "OP. GRAVADA                         ".number_format(round($data["total_price"] / 1.18 ,2),2) ."\n\n";
        $Data .= "I.S.C.              S/.              0.00\n";
        $Data .= "I.G.V.              S/.             ".round($data["total_price"] - ($data["total_price"] / 1.18) ,2) ."\n";
        $Data .= "IMPORTE TOTAL       S/.             ".number_format($data["total_price"],2)."\n";
        $Data .= "TOTAL A PAGAR       S/.             ".number_format($data["total_price"],2)."\n\n";
        $Data .= "VUELTO                              ".number_format($data["pay_change"],2)."\n";
        $printer->text($Data);

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $Data = "";
        $Data .= "******************************************\n";
        $Data .= "¡GRACIAS POR SU COMPRA!\n";
        $Data .= "VUELVA PRONTO\n";


        $printer->text($Data."\n\n\n\n");
        $printer->cut();
        $this->print_comanda($printer,$data["order_name"], $data["productos"]);
        $printer->pulse(0, 120, 240);
        $printer->close();
    }

    private function print_factura($data){
        $connector = new FilePrintConnector("//localhost/Star BSC10");
        $printer = new Printer($connector);
        //print_r($income_data_venta_printer);

        
        //Auxiliares para el texto
        date_default_timezone_set('America/Lima');
        $datetime = Time::parse($data["date"]);
        $date = $datetime->i18nFormat('yyyy/MM/dd');
        $time = $datetime->i18nFormat('HH:mm:ss');
        $max_length_prd = 22;
        $spaces_right = 0;

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $Data = "";
        $Data .= "\n";
        $Data .= "BANAWA JUICE BAR S.R.L.\n";
        $Data .= "RUC 20602896103\n";
        $Data .= "Cal.Santa Catalina Nro. 211 (Al Frente De La Alianza Francesa) Arequipa\n";
        $Data .= "Arequipa - Arequipa\n";
        $Data .= "FACTURA DE VENTA ELECTRÓNICA\n";
        $Data .= $data["cod_documento"]."\n\n";
        $printer->text($Data);

        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $Data = "";
        $Data .= "CORRELATIVO: ".$data["cod_documento"]."\n";
        $Data .= "FECHA DE EMISION: ".$date."\n";
        $Data .= "HORA: ".$time."\n";
        $Data .= "RAZON SOCIAL: ".$data["factura_razon_social"]."\n";
        $Data .= "RUC COMPRADOR: ".$data["factura_RUC"]."\n";
        $Data .= "TIPO DE MONEDA: NUEVO SOL\n\n";
        $printer->text($Data);

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $Data = "";
        $Data .= "------------------------------------------\n";
        $Data .= "CODIGO       DESCRIPCION             TOTAL\n";
        $Data .= "     CANT. X PRECIO UNIT.\n";
        $Data .= "------------------------------------------\n";
        $printer->text($Data);

        foreach ($data["productos"] as $prd_id => $amount) {
            $prd = $this->table_producto->get($prd_id);
            $prd_name = strtoupper($prd["name"]);
            if (substr($prd_name, $max_length_prd)) {
                $prd_name = substr($prd_name, 0, $max_length_prd);
            } else {
                $spaces_right = $max_length_prd - strlen($prd_name);
                $prd_name .= str_repeat(" ", $spaces_right);
            }
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text("BNW-00".$prd_id);
            $printer->text("   ".$amount." x ".$prd_name." ".number_format(round($amount*$prd["price"], 2),2)."\n");
        }
        $printer->setJustification(Printer::JUSTIFY_RIGHT);
        $Data = "";
        $Data .= "\nSUBTOTAL              S/.           ".number_format(round($data["total_price"] / 1.18 ,2),2) ."\n\n";
        $Data .= "OP. EXONERADA                        0.00\n";
        $Data .= "OP. INAFECTA                         0.00\n";
        $Data .= "OP. GRAVADA                         ".number_format(round($data["total_price"] / 1.18 ,2),2) ."\n\n";
        $Data .= "I.S.C.              S/.              0.00\n";
        $Data .= "I.G.V.              S/.             ".round($data["total_price"] - ($data["total_price"] / 1.18) ,2) ."\n";
        $Data .= "IMPORTE TOTAL       S/.             ".number_format($data["total_price"],2)."\n";
        $Data .= "TOTAL A PAGAR       S/.             ".number_format($data["total_price"],2)."\n\n";
        $Data .= "VUELTO                              ".number_format($data["pay_change"],2)."\n";
        $printer->text($Data);

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $Data = "";
        $Data .= "******************************************\n";
        $Data .= "¡GRACIAS POR SU COMPRA!\n";
        $Data .= "VUELVA PRONTO\n";


        $printer->text($Data."\n\n\n\n");
        $printer->cut();
        $this->print_comanda($printer, $data["order_name"], $data["productos"]);
        $printer->pulse(0, 120, 240);
        $printer->close();
    }


    private function print_comanda($printer, $name, $productos) {
        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $Data = "PEDIDO PARA ".$name."\n";
        $Data .= "******************************************\n";
        $printer->text($Data);

        $printer->setJustification(Printer::JUSTIFY_RIGHT);
        foreach ($productos as $prd_id => $amount) {
            $prd = $this->table_producto->get($prd_id);
            $printer->text("--> ".$prd["name"]."      ".number_format($amount, 2)."\n");
        }

        $Data = "\n";
        $Data = "******************************************";
        $printer->text($Data."\n\n\n");
        $printer->cut();
    }


    public function generateSaleReport() {
        date_default_timezone_set('America/Lima');
        $output_file_today_sale = getcwd().'\\'."reports\sale-report-".date("Y-m-d-H").".csv";
        $ventas = $this->table_venta->find("all")
            ->where(['Venta.date >=' => date('Y-m-d')." 00:00:00", 'Venta.date <=' => date("Y-m-d H:i:s")]);

        $data_to_csv = json_decode(json_encode($ventas));
        $fp = fopen($output_file_today_sale, 'w');

        $csv_fields=array();
        $csv_fields[] = 'id';
        $csv_fields[] = 'total_price';
        $csv_fields[] = 'date';
        $csv_fields[] = 'type';
        $csv_fields[] = 'is_nulled';
        $csv_fields[] = 'is_complain';
        $csv_fields[] = 'complain_detail';
        $csv_fields[] = 'null_detail';
        $csv_fields[] = 'caja_id';
        $csv_fields[] = 'descuento_id';
        $csv_fields[] = 'factura_RUC';
        $csv_fields[] = 'factura_razon_social';
        $csv_fields[] = 'order_name';
        $csv_fields[] = 'pay_amount';
        $csv_fields[] = 'pay_change';        
        fputcsv($fp, $csv_fields);

        foreach ($data_to_csv as $row) {
            fputcsv($fp, (array)$row);
        }
        fclose($fp);

        $this->response->download("sale-report-".date("Y-m-d-H").".csv");
        $file = file_get_contents($output_file_today_sale);
        print_r($file);
        //$ventas_today = $this->table_venta->find("all")
           // ->where(['Venta.date >=' => date('Y-m-d')." 00:00:00", 'Venta.date <=' => date("Y-m-d H:i:s")]);

        return $this->response;
        
        // Return the response to prevent controller from trying to render
        // a view.
    }

    private function generate_sunat_doc($venta){
        set_time_limit(1000);
        $number = new \NumberFormatter('es', \NumberFormatter::SPELLOUT);

        $util = Util::getInstance();

        // Cliente
        $client = new Client();
        $client_name = "Sin Cliente";
        $client_doc = "11111111111";
        $client_tipo_doc = "1";
        if (!empty($venta["factura_RUC"])) {
            $client_doc = $venta["factura_RUC"];
        }
        if (!empty($venta["factura_razon_social"])) {
            $client_name = $venta["factura_razon_social"];
        }
        if ($venta["type"] == "factura") {
            $client_tipo_doc = "6";
        }
        $client->setTipoDoc($client_tipo_doc)
            ->setNumDoc($client_doc)
            ->setRznSocial($client_name);

        //Empresa
        $coporation = new Company();
        $coporation ->setRuc('20602896103')
            ->setNombreComercial('BANAWA JUICE BAR S.R.L.')
            ->setRazonSocial('BANAWA JUICE BAR S.R.L.')
            ->setAddress((new Address())
            ->setDireccion('CAL.SANTA CATALINA NRO. 211 (AL FRENTE DE LA ALIANZA FRANCESA) AREQUIPA - AREQUIPA - AREQUIPA'));

        // Venta
        $cod_documento = explode("-",$venta["cod_documento"]);
        $tipo_doc = "03";
        if ($venta["type"] == "factura") {
            $tipo_doc = "01";
        } else if ($venta["type"] == "boleta") {
            $tipo_doc = "03";
        }
        $invoice = new Invoice();
        $invoice->setTipoDoc($tipo_doc)
            ->setSerie($cod_documento[0])
            ->setCorrelativo($cod_documento[1])
            ->setFechaEmision(Time::parse($venta["date"]))
            ->setTipoMoneda('PEN')
            ->setClient($client)
            ->setMtoOperGravadas(round($venta["total_price"] ,2))
            ->setMtoOperExoneradas(0)
            ->setMtoOperInafectas(0)
            ->setMtoIGV(round($venta["total_price"] - ($venta["total_price"] / 1.18) ,2))
            ->setMtoImpVenta($venta["total_price"])
            ->setCompany($coporation);

        $items = [];
        foreach ($venta["productos"] as  $prd_id => $amount) {
            $prd = $this->table_producto->find()->where(['Producto.id =' => $prd_id])->first();
            $item = new SaleDetail();
            $item->setCodProducto("BNW-00".$prd_id)
                ->setUnidad('NIU')
                ->setCantidad($amount)
                ->setDescripcion(strtoupper($prd["name"]))
                ->setIgv(round($prd["price"] - ($prd["price"] / 1.18) ,2))
                ->setTipAfeIgv('10')
                ->setMtoValorVenta(round($amount*$prd["price"], 2))
                ->setMtoValorUnitario(round($prd["price"] / 1.18 ,2))
                ->setMtoPrecioUnitario($prd["price"]);           
            array_push($items, $item);
        }

        $total_price = round($venta["total_price"], 2);
        $cents = (($total_price - intval($total_price))*100);

        if ($cents == 0) {
            $cents = strval($cents)."0";
        }
        $legend = new Legend();
        $legend->setCode('1000')
            ->setValue('SON '.strtoupper($number->format(intval($total_price))).' CON '.$cents.'/100 SOLES');
        //print_r("--->".$legend->getValue()."\n");
        $invoice->setDetails($items)
            ->setLegends([$legend]);
        
        //Envio a SUNAT.
        $see = $util->getSee("https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService");
        $res = $see->send($invoice);

        //print_r($see);
        Util::writeXml($invoice, $see->getFactory()->getLastXml());
        if ($res->isSuccess()) {
            /**@var $res \Greenter\Model\Response\BillResult*/
            $cdr = $res->getCdrResponse();
            Util::writeCdr($invoice, $res->getCdrZip());
            //echo $util->getResponseFromCdr($cdr);
            return "Éxito ".$venta["cod_documento"];
        } else {
            //var_dump($res->getError());
            return "Falla ".$venta["cod_documento"];
        }
    }

    public function generateSunatDocs() {
        set_time_limit(1000);
        $ventas = $this->table_venta->find()->contain(['Producto']);
        $number = new \NumberFormatter('es', \NumberFormatter::SPELLOUT);
        $see = new \Greenter\See();
        $see->setService(SunatEndpoints::FE_PRODUCCION);
        $see->setCertificate(file_get_contents(ROOT .DS. 'cert' . DS  . 'banawa-cert.pem'));
        $see->setCredentials('PALLIN00', 'amademage');

        foreach ($ventas as $venta) {

            // Cliente
            $client = new Client();
            $client_name = "Sin Cliente";
            $client_doc = "11111111111";
            $client_tipo_doc = "1";
            if (!empty($venta["factura_RUC"])) {
                $client_doc = $venta["factura_RUC"];
            }
            if (!empty($venta["factura_razon_social"])) {
                $client_name = $venta["factura_razon_social"];
            }
            if ($venta["type"] == "factura") {
                $client_tipo_doc = "6";
            }
            $client->setTipoDoc($client_tipo_doc)
                ->setNumDoc($client_doc)
                ->setRznSocial($client_name);

            $address = new Address();
            $address->setUbigueo('040101')
                ->setDepartamento('AREQUIPA')
                ->setProvincia('AREQUIPA')
                ->setDistrito('AREQUIPA')
                ->setUrbanizacion('NONE')
                ->setDireccion('CAL. SANTA CATALINA 211');

            $coporation = new Company();
            $coporation->setRuc('20602896103')
                ->setNombreComercial('BANAWA JUICE BAR S.R.L.')
                ->setRazonSocial('BANAWA JUICE BAR S.R.L.')
                ->setAddress($address);

            //Empresa
            // $coporation = new Company();
            // $coporation ->setRuc('20602896103')
            //     ->setNombreComercial('BANAWA JUICE BAR S.R.L.')
            //     ->setRazonSocial('BANAWA JUICE BAR S.R.L.')
            //     ->setAddress((new Address())
            //     ->setDireccion('CAL.SANTA CATALINA NRO. 211 (AL FRENTE DE LA ALIANZA FRANCESA) AREQUIPA - AREQUIPA - AREQUIPA'));

            // Venta
            $cod_documento = explode("-",$venta["cod_documento"]);
            $tipo_doc = "03";
            if ($venta["type"] == "factura") {
                $tipo_doc = "01";
            } else if ($venta["type"] == "boleta") {
                $tipo_doc = "03";
            }
            $invoice = new Invoice();
            $invoice->setTipoDoc($tipo_doc)
                ->setSerie($cod_documento[0])
                ->setCorrelativo($cod_documento[1])
                ->setFechaEmision(Time::parse($venta["date"]))
                ->setTipoMoneda('PEN')
                ->setClient($client)
                ->setMtoOperGravadas(round($venta["total_price"],2))
                ->setMtoOperExoneradas(0)
                ->setMtoOperInafectas(0)
                ->setMtoIGV(round($venta["total_price"] - ($venta["total_price"] / 1.18) ,2))
                ->setMtoImpVenta($venta["total_price"])
                ->setCompany($coporation);


            $items = [];           
            foreach ($venta["producto"] as $row) {
                $item = new SaleDetail();
                $item->setCodProducto("BNW-00".$row["id"])
                    ->setUnidad('NIU')
                    ->setCantidad($row["_joinData"]["amount"])
                    ->setDescripcion(strtoupper($row["name"]))
                    ->setIgv(18.00)
                    ->setTipAfeIgv('10')
                    ->setMtoValorVenta(round($row["_joinData"]["amount"]*$row["price"], 2))
                    ->setMtoValorUnitario(round($row["price"] - ($row["price"] / 1.18) ,2))
                    ->setMtoPrecioUnitario($row["price"]);
                array_push($items,$item);
            }

            $total_price = round($venta["total_price"], 2);
            $cents = (($total_price - intval($total_price))*100);

            if ($cents == 0) {
                $cents = strval($cents)."0";
            }

            $legend = new Legend();
            $legend->setCode('1000')
                ->setValue('SON '.strtoupper($number->format(intval($total_price))).' CON '.$cents.'/100 SOLES');
            //print_r("--->".$legend->getValue()."\n");
            $invoice->setDetails($items)
                ->setLegends([$legend]);
            
            //Envio a SUNAT.
            $result = $see->send($invoice);

            if ($result->isSuccess()) {
                echo $result->getCdrResponse()->getDescription();
            } else {
                var_dump($result->getError());
            }
        }
    }

    public function testGreenter() {

        $see = new \Greenter\See();
        $see->setService(SunatEndpoints::FE_BETA);
        $see->setCertificate(file_get_contents(ROOT .DS. 'cert' . DS  . 'banawa-cert.pem'));
        $see->setCredentials('20000000001MODDATOS', 'moddatos');

        $client = new Client();
        $client->setTipoDoc('6')
            ->setNumDoc('20000000001')
            ->setRznSocial('EMPRESA 1');

        // Emisor
        $address = new Address();
        $address->setUbigueo('150101')
            ->setDepartamento('LIMA')
            ->setProvincia('LIMA')
            ->setDistrito('LIMA')
            ->setUrbanizacion('NONE')
            ->setDireccion('AV LS');

        $company = new Company();
        $company->setRuc('20000000001')
            ->setRazonSocial('EMPRESA SAC')
            ->setNombreComercial('EMPRESA')
            ->setAddress($address);

        // Venta
        $invoice = (new Invoice())
            ->setTipoDoc('01')
            ->setSerie('F001')
            ->setCorrelativo('1')
            ->setFechaEmision(new \DateTime())
            ->setTipoMoneda('PEN')
            ->setClient($client)
            ->setMtoOperGravadas(200.00)
            ->setMtoOperExoneradas(0.00)
            ->setMtoOperInafectas(0.00)
            ->setMtoIGV(36.00)
            ->setMtoImpVenta(2236.00)
            ->setCompany($company);

        $item = (new SaleDetail())
            ->setCodProducto('P001')
            ->setUnidad('NIU')
            ->setCantidad(2)
            ->setDescripcion('PRODUCTO 1')
            ->setIgv(18.00)
            ->setTipAfeIgv('10')
            ->setMtoValorVenta(100.00)
            ->setMtoValorUnitario(50.00)
            ->setMtoPrecioUnitario(56.00);

        $legend = (new Legend())
            ->setCode('1000')
            ->setValue('SON DOSCIENTOS TREINTA Y SEIS CON 00/100 SOLES');

        $invoice->setDetails([$item])
                ->setLegends([$legend]);

        $result = $see->send($invoice);

        if ($result->isSuccess()) {
            echo $result->getCdrResponse()->getDescription();
        } else {
            var_dump($result->getError());
        }
    }

    public function generateSunatCorrelatives(){
        $ventas = $this->table_venta->find()->contain(['Producto']);
        $number = new \NumberFormatter('es', \NumberFormatter::SPELLOUT);
        $cod_boleta = $cod_factura = 1;

        foreach ($ventas as $venta) {
            if ($venta["type"] == "boleta") {
                $venta["cod_documento"] = "B001-".$cod_boleta;
                $cod_boleta += 1;
            } else if ($venta["type"] == "factura") {
                $venta["cod_documento"] = "F001-".$cod_factura;
                $cod_factura += 1;
            }
            $new_venta = $this->table_venta->patchEntity($venta, $this->request->getData());
            if ($this->table_venta->save($new_venta)) {
                print_r("Éxito ".$new_venta["cod_documento"]);
            } else {
                print_r("Falla ".$new_venta["cod_documento"]);
            }
            
        }
    }

    public function printDocumentDuplicate($sale_id = -1){
        if ($sale_id != -1) { 
            $venta = $this->table_venta->find("all")
                ->where(['Venta.id =' => $sale_id])
                ->contain(['Producto'])
                ->last();
            if (!empty($venta) && $venta["type"] == "factura") {
                $venta["productos"] = [];
                foreach($venta["producto"] as $prd) {
                    $venta["productos"][$prd->id] = $prd->_joinData->amount;
                }
                $this->print_factura($venta);
            } else if (!empty($venta) && $venta["type"] == "boleta") {
                $venta["productos"] = [];
                foreach($venta["producto"] as $prd) {
                    $venta["productos"][$prd->id] = $prd->_joinData->amount;
                }
                $this->print_boleta($venta);
            }
        }
        return;
    }
}
