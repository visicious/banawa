<?php
namespace App\Controller;
 
use App\Controller\AppController;
use Cake\Core\App;
require_once(ROOT .DS. 'vendor' . DS  . 'fusioncharts' . DS . 'fusioncharts.php');
use Cake\ORM\TableRegistry;

 
class ChartsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->table_producto = TableRegistry::get('Producto');
        $this->table_venta_producto = TableRegistry::get('VentaProducto');
    }

    public function index()
    {

        $query = $this->table_venta_producto->find()->contain(['Producto']);
        $this->set(compact('query'));
    }
}