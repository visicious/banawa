<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Libraries\TicketPrinter;
use Cake\ORM\TableRegistry;

use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;


class HomeController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->table_producto = TableRegistry::get('Producto');
        $this->table_categorias = TableRegistry::get('Categoria');
        $this->table_caja = TableRegistry::get('Caja');
    }

    public function index()
    {
        $password = "Credenciales de la sesion";
        $this->viewBuilder()->setLayout("home");

        $available_categories = $this->paginate($this->table_categorias);
        //$available_categories = [];

        $actual_caja = $this->table_caja->get("1")->toArray();
        //print_r($actual_caja);

        // foreach ($productos as $producto) {
        //     if($producto->has('categorium')) {
        //         $available_categories[h($producto->categorium->name)] = $producto->categorium;
        //     } 
        // }

        //print_r($available_categories);
        //$this->test_printer();
        $this->set(compact('available_categories'));
        $this->set(compact('actual_caja'));
        $this->set(compact('password'));
    }

    // /**
    //  * View method
    //  *
    //  * @param string|null $id Caja id.
    //  * @return \Cake\Http\Response|void
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function view($id = null)
    // {
    //     $caja = $this->Caja->get($id, [
    //         'contain' => ['Empleado', 'Venta']
    //     ]);

    //     $this->set('caja', $caja);
    // }

    // /**
    //  * Add method
    //  *
    //  * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
    //  */
    // public function add()
    // {
    //     $caja = $this->Caja->newEntity();
    //     if ($this->request->is('post')) {
    //         $caja = $this->Caja->patchEntity($caja, $this->request->getData());

    //         $caja->password = null;
    //         if (!empty($caja->original_password)) {
    //             $caja->password = $this->_setHashedPassword($caja->original_password);
    //         }

    //         if ($this->Caja->save($caja)) {
    //             $this->Flash->success(__('The caja has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The caja could not be saved. Please, try again.'));
    //     }
    //     $empleado = $this->Caja->Empleado->find('list', ['limit' => 200]);
    //     $this->set(compact('caja', 'empleado'));
    // }

    // /**
    //  * Edit method
    //  *
    //  * @param string|null $id Caja id.
    //  * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
    //  * @throws \Cake\Network\Exception\NotFoundException When record not found.
    //  */
    // public function edit($id = null)
    // {
    //     $caja = $this->Caja->get($id, [
    //         'contain' => ['Empleado']
    //     ]);

    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $caja = $this->Caja->patchEntity($caja, $this->request->getData());

    //         $caja->password = null;
    //         if (!empty($caja->original_password)) {
    //             $caja->password = $this->_setHashedPassword($caja->original_password);
    //         }

    //         if ($this->Caja->save($caja)) {
    //             $this->Flash->success(__('The caja has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The caja could not be saved. Please, try again.'));
    //     }
    //     $empleado = $this->Caja->Empleado->find('list', ['limit' => 200]);
    //     $this->set(compact('caja', 'empleado'));
    // }

    // /**
    //  * Delete method
    //  *
    //  * @param string|null $id Caja id.
    //  * @return \Cake\Http\Response|null Redirects to index.
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $caja = $this->Caja->get($id);
    //     if ($this->Caja->delete($caja)) {
    //         $this->Flash->success(__('The caja has been deleted.'));
    //     } else {
    //         $this->Flash->error(__('The caja could not be deleted. Please, try again.'));
    //     }

    //     return $this->redirect(['action' => 'index']);
    // }

    // protected function _setHashedPassword($value)
    // {
    //     $hasher = new DefaultPasswordHasher();
    //     return $hasher->hash($value);
    // }
}
