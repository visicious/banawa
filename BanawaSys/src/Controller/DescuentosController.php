<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Descuentos Controller
 *
 * @property \App\Model\Table\DescuentosTable $Descuentos
 *
 * @method \App\Model\Entity\Descuento[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DescuentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $descuentos = $this->paginate($this->Descuentos);

        $this->set(compact('descuentos'));
    }

    /**
     * View method
     *
     * @param string|null $id Descuento id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $descuento = $this->Descuentos->get($id, [
            'contain' => ['Producto', 'Venta']
        ]);

        $this->set('descuento', $descuento);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $descuento = $this->Descuentos->newEntity();
        if ($this->request->is('post')) {
            $descuento = $this->Descuentos->patchEntity($descuento, $this->request->getData());
            if ($this->Descuentos->save($descuento)) {
                $this->Flash->success(__('The descuento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The descuento could not be saved. Please, try again.'));
        }
        $this->set(compact('descuento'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Descuento id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $descuento = $this->Descuentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $descuento = $this->Descuentos->patchEntity($descuento, $this->request->getData());
            if ($this->Descuentos->save($descuento)) {
                $this->Flash->success(__('The descuento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The descuento could not be saved. Please, try again.'));
        }
        $this->set(compact('descuento'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Descuento id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $descuento = $this->Descuentos->get($id);
        if ($this->Descuentos->delete($descuento)) {
            $this->Flash->success(__('The descuento has been deleted.'));
        } else {
            $this->Flash->error(__('The descuento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
