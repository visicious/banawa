<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Insumo Controller
 *
 * @property \App\Model\Table\InsumoTable $Insumo
 *
 * @method \App\Model\Entity\Insumo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InsumoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $insumo = $this->paginate($this->Insumo);

        $this->set(compact('insumo'));
    }

    /**
     * View method
     *
     * @param string|null $id Insumo id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $insumo = $this->Insumo->get($id, [
            'contain' => ['Receta']
        ]);

        $this->set('insumo', $insumo);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $insumo = $this->Insumo->newEntity();
        if ($this->request->is('post')) {
            $insumo = $this->Insumo->patchEntity($insumo, $this->request->getData());
            if ($this->Insumo->save($insumo)) {
                $this->Flash->success(__('The insumo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The insumo could not be saved. Please, try again.'));
        }
        $this->set(compact('insumo'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Insumo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $insumo = $this->Insumo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $insumo = $this->Insumo->patchEntity($insumo, $this->request->getData());
            if ($this->Insumo->save($insumo)) {
                $this->Flash->success(__('The insumo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The insumo could not be saved. Please, try again.'));
        }
        $this->set(compact('insumo'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Insumo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $insumo = $this->Insumo->get($id);
        if ($this->Insumo->delete($insumo)) {
            $this->Flash->success(__('The insumo has been deleted.'));
        } else {
            $this->Flash->error(__('The insumo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
