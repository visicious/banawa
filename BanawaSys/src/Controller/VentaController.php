<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Venta Controller
 *
 * @property \App\Model\Table\VentaTable $Venta
 *
 * @method \App\Model\Entity\Ventum[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VentaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Caja', 'Descuentos']
        ];
        $venta = $this->paginate($this->Venta);

        $this->set(compact('venta'));
    }

    /**
     * View method
     *
     * @param string|null $id Ventum id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $venta = $this->Venta->get($id, [
            'contain' => ['Caja', 'Descuentos', 'Producto']
        ]);

        $this->set('venta', $venta);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $venta = $this->Venta->newEntity();
        if ($this->request->is('post')) {
            $venta = $this->Venta->patchEntity($venta, $this->request->getData());
            if ($this->Venta->save($venta)) {
                $this->Flash->success(__('The venta has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The venta could not be saved. Please, try again.'));
        }
        $caja = $this->Venta->Caja->find('list', ['limit' => 200]);
        $descuentos = $this->Venta->Descuentos->find('list', ['limit' => 200]);
        $producto = $this->Venta->Producto->find('list', ['limit' => 200]);
        $this->set(compact('venta', 'caja', 'descuentos', 'producto'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Ventum id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $venta = $this->Venta->get($id, [
            'contain' => ['Producto']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $venta = $this->Venta->patchEntity($venta, $this->request->getData());
            if ($this->Venta->save($venta)) {
                $this->Flash->success(__('The venta has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The venta could not be saved. Please, try again.'));
        }
        $caja = $this->Venta->Caja->find('list', ['limit' => 200]);
        $descuentos = $this->Venta->Descuentos->find('list', ['limit' => 200]);
        $producto = $this->Venta->Producto->find('list', ['limit' => 200]);
        $this->set(compact('venta', 'caja', 'descuentos', 'producto'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Ventum id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $venta = $this->Venta->get($id);
        if ($this->Venta->delete($venta)) {
            $this->Flash->success(__('The venta has been deleted.'));
        } else {
            $this->Flash->error(__('The venta could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
