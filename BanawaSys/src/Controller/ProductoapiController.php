<?php
namespace App\Controller;

use App\Controller\AppapiController;
use Cake\ORM\TableRegistry;

class ProductoapiController extends AppapiController
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->table_producto = TableRegistry::get('Producto');
    }

    public function index()
    {
        $this->paginate = [
            'contain' => ['Descuentos', 'Categoria']
        ];
        $producto = $this->paginate($this->table_producto);

        $this->set([
            'producto' => $producto,
            '_serialize' => ['producto']
        ]);
    }

    public function getPrdByCat($cat_id = null)
    {
        $productos = $this->table_producto->find("all")
            ->where(['Producto.categoria_id =' => $cat_id, 'Producto.active =' => 1])
            ->contain(['Descuentos', 'Categoria', 'Receta']);

        $this->set([
            'productos' => $productos,
            '_serialize' => ['productos']
        ]);
    }

    /**
     * View method
     *
     * @param string|null $id Producto id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $producto = $this->table_producto->get($id, [
            'contain' => ['Descuentos', 'Categoria', 'Venta', 'Receta']
        ]);

        $this->set([
            'producto' => $producto,
            '_serialize' => ['producto']
        ]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $producto = $this->table_producto->newEntity();
        if ($this->request->is('post')) {
            $producto = $this->table_producto->patchEntity($producto, $this->request->getData());
            if ($this->table_producto->save($producto)) {
                $this->Flash->success(__('The producto has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The producto could not be saved. Please, try again.'));
        }
        $descuentos = $this->table_producto->Descuentos->find('list', ['limit' => 200]);
        $categoria = $this->table_producto->Categoria->find('list', ['limit' => 200]);
        $venta = $this->table_producto->Venta->find('list', ['limit' => 200]);
        $this->set(compact('producto', 'descuentos', 'categoria', 'venta'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Producto id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $producto = $this->table_producto->get($id, [
            'contain' => ['Venta']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $producto = $this->table_producto->patchEntity($producto, $this->request->getData());
            if ($this->table_producto->save($producto)) {
                $this->Flash->success(__('The producto has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The producto could not be saved. Please, try again.'));
        }
        $descuentos = $this->table_producto->Descuentos->find('list', ['limit' => 200]);
        $categoria = $this->table_producto->Categoria->find('list', ['limit' => 200]);
        $venta = $this->table_producto->Venta->find('list', ['limit' => 200]);
        $this->set(compact('producto', 'descuentos', 'categoria', 'venta'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Producto id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $producto = $this->table_producto->get($id);
        if ($this->table_producto->delete($producto)) {
            $this->Flash->success(__('The producto has been deleted.'));
        } else {
            $this->Flash->error(__('The producto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
