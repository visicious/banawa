<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * VentaProducto Controller
 *
 * @property \App\Model\Table\VentaProductoTable $VentaProducto
 *
 * @method \App\Model\Entity\VentaProducto[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VentaProductoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Producto', 'Venta']
        ];
        $ventaProducto = $this->paginate($this->VentaProducto);

        $this->set(compact('ventaProducto'));
    }

    /**
     * View method
     *
     * @param string|null $id Venta Producto id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ventaProducto = $this->VentaProducto->get($id, [
            'contain' => ['Producto', 'Venta']
        ]);

        $this->set('ventaProducto', $ventaProducto);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ventaProducto = $this->VentaProducto->newEntity();
        if ($this->request->is('post')) {
            $ventaProducto = $this->VentaProducto->patchEntity($ventaProducto, $this->request->getData());
            if ($this->VentaProducto->save($ventaProducto)) {
                $this->Flash->success(__('The venta producto has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The venta producto could not be saved. Please, try again.'));
        }
        $producto = $this->VentaProducto->Producto->find('list', ['limit' => 200]);
        $venta = $this->VentaProducto->Venta->find('list', ['limit' => 200]);
        $this->set(compact('ventaProducto', 'producto', 'venta'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Venta Producto id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ventaProducto = $this->VentaProducto->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ventaProducto = $this->VentaProducto->patchEntity($ventaProducto, $this->request->getData());
            if ($this->VentaProducto->save($ventaProducto)) {
                $this->Flash->success(__('The venta producto has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The venta producto could not be saved. Please, try again.'));
        }
        $producto = $this->VentaProducto->Producto->find('list', ['limit' => 200]);
        $venta = $this->VentaProducto->Venta->find('list', ['limit' => 200]);
        $this->set(compact('ventaProducto', 'producto', 'venta'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Venta Producto id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ventaProducto = $this->VentaProducto->get($id);
        if ($this->VentaProducto->delete($ventaProducto)) {
            $this->Flash->success(__('The venta producto has been deleted.'));
        } else {
            $this->Flash->error(__('The venta producto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
