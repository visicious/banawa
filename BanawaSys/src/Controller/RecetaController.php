<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Receta Controller
 *
 * @property \App\Model\Table\RecetaTable $Receta
 *
 * @method \App\Model\Entity\Recetum[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RecetaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Insumo', 'Producto']
        ];
        $receta = $this->paginate($this->Receta);

        $this->set(compact('receta'));
    }

    /**
     * View method
     *
     * @param string|null $id Recetum id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $recetum = $this->Receta->get($id, [
            'contain' => ['Insumo', 'Producto']
        ]);

        $this->set('recetum', $recetum);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $recetum = $this->Receta->newEntity();
        if ($this->request->is('post')) {
            $recetum = $this->Receta->patchEntity($recetum, $this->request->getData());
            if ($this->Receta->save($recetum)) {
                $this->Flash->success(__('The recetum has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The recetum could not be saved. Please, try again.'));
        }
        $insumo = $this->Receta->Insumo->find('list', ['limit' => 200]);
        $producto = $this->Receta->Producto->find('list', ['limit' => 200]);
        $this->set(compact('recetum', 'insumo', 'producto'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Recetum id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $recetum = $this->Receta->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $recetum = $this->Receta->patchEntity($recetum, $this->request->getData());
            if ($this->Receta->save($recetum)) {
                $this->Flash->success(__('The recetum has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The recetum could not be saved. Please, try again.'));
        }
        $insumo = $this->Receta->Insumo->find('list', ['limit' => 200]);
        $producto = $this->Receta->Producto->find('list', ['limit' => 200]);
        $this->set(compact('recetum', 'insumo', 'producto'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Recetum id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $recetum = $this->Receta->get($id);
        if ($this->Receta->delete($recetum)) {
            $this->Flash->success(__('The recetum has been deleted.'));
        } else {
            $this->Flash->error(__('The recetum could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
