<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Caja Controller
 *
 * @property \App\Model\Table\CajaTable $Caja
 *
 * @method \App\Model\Entity\Caja[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CajaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $caja = $this->paginate($this->Caja);

        $this->set(compact('caja'));
    }

    /**
     * View method
     *
     * @param string|null $id Caja id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $caja = $this->Caja->get($id, [
            'contain' => ['Empleado', 'Venta']
        ]);

        $this->set('caja', $caja);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $caja = $this->Caja->newEntity();
        if ($this->request->is('post')) {
            $caja = $this->Caja->patchEntity($caja, $this->request->getData());

            $caja->password = null;
            if (!empty($caja->original_password)) {
                $caja->password = $this->_setHashedPassword($caja->original_password);
            }

            if ($this->Caja->save($caja)) {
                $this->Flash->success(__('The caja has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The caja could not be saved. Please, try again.'));
        }
        $empleado = $this->Caja->Empleado->find('list', ['limit' => 200]);
        $this->set(compact('caja', 'empleado'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Caja id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $caja = $this->Caja->get($id, [
            'contain' => ['Empleado']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $caja = $this->Caja->patchEntity($caja, $this->request->getData());

            $caja->password = null;
            if (!empty($caja->original_password)) {
                $caja->password = $this->_setHashedPassword($caja->original_password);
            }

            if ($this->Caja->save($caja)) {
                $this->Flash->success(__('The caja has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The caja could not be saved. Please, try again.'));
        }
        $empleado = $this->Caja->Empleado->find('list', ['limit' => 200]);
        $this->set(compact('caja', 'empleado'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Caja id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $caja = $this->Caja->get($id);
        if ($this->Caja->delete($caja)) {
            $this->Flash->success(__('The caja has been deleted.'));
        } else {
            $this->Flash->error(__('The caja could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    protected function _setHashedPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($value);
        }
    }
}
