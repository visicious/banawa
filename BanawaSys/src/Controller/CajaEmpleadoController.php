<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CajaEmpleado Controller
 *
 * @property \App\Model\Table\CajaEmpleadoTable $CajaEmpleado
 *
 * @method \App\Model\Entity\CajaEmpleado[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CajaEmpleadoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Empleado', 'Caja']
        ];
        $cajaEmpleado = $this->paginate($this->CajaEmpleado);

        $this->set(compact('cajaEmpleado'));
    }

    /**
     * View method
     *
     * @param string|null $id Caja Empleado id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cajaEmpleado = $this->CajaEmpleado->get($id, [
            'contain' => ['Empleado', 'Caja']
        ]);

        $this->set('cajaEmpleado', $cajaEmpleado);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cajaEmpleado = $this->CajaEmpleado->newEntity();
        if ($this->request->is('post')) {
            $cajaEmpleado = $this->CajaEmpleado->patchEntity($cajaEmpleado, $this->request->getData());
            if ($this->CajaEmpleado->save($cajaEmpleado)) {
                $this->Flash->success(__('The caja empleado has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The caja empleado could not be saved. Please, try again.'));
        }
        $empleado = $this->CajaEmpleado->Empleado->find('list', ['limit' => 200]);
        $caja = $this->CajaEmpleado->Caja->find('list', ['limit' => 200]);
        $this->set(compact('cajaEmpleado', 'empleado', 'caja'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Caja Empleado id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cajaEmpleado = $this->CajaEmpleado->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cajaEmpleado = $this->CajaEmpleado->patchEntity($cajaEmpleado, $this->request->getData());
            if ($this->CajaEmpleado->save($cajaEmpleado)) {
                $this->Flash->success(__('The caja empleado has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The caja empleado could not be saved. Please, try again.'));
        }
        $empleado = $this->CajaEmpleado->Empleado->find('list', ['limit' => 200]);
        $caja = $this->CajaEmpleado->Caja->find('list', ['limit' => 200]);
        $this->set(compact('cajaEmpleado', 'empleado', 'caja'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Caja Empleado id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cajaEmpleado = $this->CajaEmpleado->get($id);
        if ($this->CajaEmpleado->delete($cajaEmpleado)) {
            $this->Flash->success(__('The caja empleado has been deleted.'));
        } else {
            $this->Flash->error(__('The caja empleado could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
