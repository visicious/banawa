<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Insumo Model
 *
 * @property \App\Model\Table\RecetaTable|\Cake\ORM\Association\HasMany $Receta
 *
 * @method \App\Model\Entity\Insumo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Insumo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Insumo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Insumo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Insumo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Insumo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Insumo findOrCreate($search, callable $callback = null, $options = [])
 */
class InsumoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('insumo');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Receta', [
            'foreignKey' => 'insumo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('quantity')
            ->requirePresence('quantity', 'create')
            ->notEmpty('quantity');

        $validator
            ->scalar('measure_unity')
            ->maxLength('measure_unity', 50)
            ->requirePresence('measure_unity', 'create')
            ->notEmpty('measure_unity');

        $validator
            ->numeric('buy_amount')
            ->requirePresence('buy_amount', 'create')
            ->notEmpty('buy_amount');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->numeric('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->date('buy_date')
            ->requirePresence('buy_date', 'create')
            ->notEmpty('buy_date');

        $validator
            ->scalar('provider')
            ->maxLength('provider', 150)
            ->requirePresence('provider', 'create')
            ->notEmpty('provider');

        $validator
            ->numeric('waste')
            ->requirePresence('waste', 'create')
            ->notEmpty('waste');

        $validator
            ->numeric('avalible_items')
            ->requirePresence('avalible_items', 'create')
            ->notEmpty('avalible_items');

        return $validator;
    }
}
