<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Receta Model
 *
 * @property \App\Model\Table\InsumoTable|\Cake\ORM\Association\BelongsTo $Insumo
 * @property \App\Model\Table\ProductoTable|\Cake\ORM\Association\BelongsTo $Producto
 *
 * @method \App\Model\Entity\Recetum get($primaryKey, $options = [])
 * @method \App\Model\Entity\Recetum newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Recetum[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Recetum|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Recetum patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Recetum[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Recetum findOrCreate($search, callable $callback = null, $options = [])
 */
class RecetaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('receta');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Insumo', [
            'foreignKey' => 'insumo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Producto', [
            'foreignKey' => 'producto_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('quantity')
            ->requirePresence('quantity', 'create')
            ->notEmpty('quantity');

        $validator
            ->scalar('measure_unity')
            ->maxLength('measure_unity', 50)
            ->requirePresence('measure_unity', 'create')
            ->notEmpty('measure_unity');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['insumo_id'], 'Insumo'));
        $rules->add($rules->existsIn(['producto_id'], 'Producto'));

        return $rules;
    }
}
