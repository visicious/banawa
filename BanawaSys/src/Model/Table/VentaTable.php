<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Venta Model
 *
 * @property \App\Model\Table\CajaTable|\Cake\ORM\Association\BelongsTo $Caja
 * @property \App\Model\Table\DescuentosTable|\Cake\ORM\Association\BelongsTo $Descuentos
 * @property \App\Model\Table\ProductoTable|\Cake\ORM\Association\BelongsToMany $Producto
 *
 * @method \App\Model\Entity\Venta get($primaryKey, $options = [])
 * @method \App\Model\Entity\Venta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Venta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Venta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Venta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Venta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Venta findOrCreate($search, callable $callback = null, $options = [])
 */
class VentaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('venta');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Caja', [
            'foreignKey' => 'caja_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Descuentos', [
            'foreignKey' => 'descuento_id'
        ]);
        $this->belongsToMany('Producto', [
            'foreignKey' => 'venta_id',
            'targetForeignKey' => 'producto_id',
            'joinTable' => 'venta_producto'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('total_price')
            ->requirePresence('total_price', 'create')
            ->notEmpty('total_price');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->scalar('type')
            ->maxLength('type', 15)
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->boolean('is_nulled')
            ->allowEmpty('is_nulled');

        $validator
            ->boolean('is_complain')
            ->allowEmpty('is_complain');

        $validator
            ->scalar('complain_detail')
            ->allowEmpty('complain_detail');

        $validator
            ->scalar('null_detail')
            ->allowEmpty('null_detail');

        $validator
            ->scalar('factura_RUC')
            ->allowEmpty('factura_RUC');

        $validator
            ->scalar('factura_razon_social')
            ->allowEmpty('factura_razon_social');

        $validator
            ->scalar('order_name')
            ->allowEmpty('order_name');

        $validator
            ->numeric('pay_amount')
            ->requirePresence('pay_amount', 'create')
            ->notEmpty('pay_amount');

        $validator
            ->numeric('pay_change')
            ->requirePresence('pay_change', 'create')
            ->notEmpty('pay_change');

        $validator
            ->scalar('cod_documento')
            ->requirePresence('cod_documento', 'create')
            ->notEmpty('cod_documento');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['caja_id'], 'Caja'));
        $rules->add($rules->existsIn(['descuento_id'], 'Descuentos'));

        return $rules;
    }
}
