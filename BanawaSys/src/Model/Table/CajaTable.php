<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Caja Model
 *
 * @property \App\Model\Table\VentaTable|\Cake\ORM\Association\HasMany $Venta
 * @property \App\Model\Table\EmpleadoTable|\Cake\ORM\Association\BelongsToMany $Empleado
 *
 * @method \App\Model\Entity\Caja get($primaryKey, $options = [])
 * @method \App\Model\Entity\Caja newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Caja[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Caja|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Caja patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Caja[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Caja findOrCreate($search, callable $callback = null, $options = [])
 */
class CajaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('caja');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Venta', [
            'foreignKey' => 'caja_id'
        ]);
        $this->belongsToMany('Empleado', [
            'foreignKey' => 'caja_id',
            'targetForeignKey' => 'empleado_id',
            'joinTable' => 'caja_empleado'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 70)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('type')
            ->maxLength('type', 15)
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->scalar('user')
            ->maxLength('user', 50)
            ->requirePresence('user', 'create')
            ->notEmpty('user');

        $validator
            ->scalar('password')
            ->maxLength('password', 50)
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->scalar('original_password')
            ->maxLength('original_password', 50)
            ->requirePresence('original_password', 'create')
            ->notEmpty('original_password');

        $validator
            ->scalar('location')
            ->maxLength('location', 150)
            ->requirePresence('location', 'create')
            ->notEmpty('location');

        $validator
            ->requirePresence('workers_amount', 'create')
            ->notEmpty('workers_amount');

        return $validator;
    }
}
