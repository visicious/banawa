<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CajaEmpleado Model
 *
 * @property \App\Model\Table\EmpleadoTable|\Cake\ORM\Association\BelongsTo $Empleado
 * @property \App\Model\Table\CajaTable|\Cake\ORM\Association\BelongsTo $Caja
 *
 * @method \App\Model\Entity\CajaEmpleado get($primaryKey, $options = [])
 * @method \App\Model\Entity\CajaEmpleado newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CajaEmpleado[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CajaEmpleado|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CajaEmpleado patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CajaEmpleado[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CajaEmpleado findOrCreate($search, callable $callback = null, $options = [])
 */
class CajaEmpleadoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('caja_empleado');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Empleado', [
            'foreignKey' => 'empleado_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Caja', [
            'foreignKey' => 'caja_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('hire_date')
            ->requirePresence('hire_date', 'create')
            ->notEmpty('hire_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['empleado_id'], 'Empleado'));
        $rules->add($rules->existsIn(['caja_id'], 'Caja'));

        return $rules;
    }
}
