<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Descuentos Model
 *
 * @property \App\Model\Table\ProductoTable|\Cake\ORM\Association\HasMany $Producto
 * @property \App\Model\Table\VentaTable|\Cake\ORM\Association\HasMany $Venta
 *
 * @method \App\Model\Entity\Descuento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Descuento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Descuento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Descuento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Descuento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Descuento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Descuento findOrCreate($search, callable $callback = null, $options = [])
 */
class DescuentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('descuentos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Producto', [
            'foreignKey' => 'descuento_id'
        ]);
        $this->hasMany('Venta', [
            'foreignKey' => 'descuento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('type')
            ->maxLength('type', 15)
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->numeric('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->integer('quantity')
            ->requirePresence('quantity', 'create')
            ->notEmpty('quantity');

        $validator
            ->integer('avalible_amount')
            ->requirePresence('avalible_amount', 'create')
            ->notEmpty('avalible_amount');

        return $validator;
    }
}
