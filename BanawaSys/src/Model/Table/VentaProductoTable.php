<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * VentaProducto Model
 *
 * @property \App\Model\Table\ProductoTable|\Cake\ORM\Association\BelongsTo $Producto
 * @property \App\Model\Table\VentaTable|\Cake\ORM\Association\BelongsTo $Venta
 *
 * @method \App\Model\Entity\VentaProducto get($primaryKey, $options = [])
 * @method \App\Model\Entity\VentaProducto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\VentaProducto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\VentaProducto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VentaProducto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\VentaProducto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\VentaProducto findOrCreate($search, callable $callback = null, $options = [])
 */
class VentaProductoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('venta_producto');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Producto', [
            'foreignKey' => 'producto_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Venta', [
            'foreignKey' => 'venta_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('amount')
            ->requirePresence('amount', 'create')
            ->notEmpty('amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['producto_id'], 'Producto'));
        $rules->add($rules->existsIn(['venta_id'], 'Venta'));

        return $rules;
    }
}
