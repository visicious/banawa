<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Producto Entity
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property string $thumbnail
 * @property int $descuento_id
 * @property int $categoria_id
 *
 * @property \App\Model\Entity\Descuento $descuento
 * @property \App\Model\Entity\Categorium $categorium
 * @property \App\Model\Entity\Recetum[] $receta
 * @property \App\Model\Entity\Venta[] $venta
 * @property \App\Model\Entity\VentaProducto[] $venta_producto
 */
class Producto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'price' => true,
        'thumbnail' => true,
        'descuento_id' => true,
        'categoria_id' => true,
        'descuento' => true,
        'active' => true,
        'categorium' => true,
        'receta' => true,
        'venta' => true,
        'venta_producto' => true
    ];
}
