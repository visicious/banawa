<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Recetum Entity
 *
 * @property int $id
 * @property float $quantity
 * @property string $measure_unity
 * @property int $insumo_id
 * @property int $producto_id
 *
 * @property \App\Model\Entity\Insumo $insumo
 * @property \App\Model\Entity\Producto $producto
 */
class Recetum extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'quantity' => true,
        'measure_unity' => true,
        'insumo_id' => true,
        'producto_id' => true,
        'insumo' => true,
        'producto' => true
    ];
}
