<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CajaEmpleado Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $hire_date
 * @property int $empleado_id
 * @property int $caja_id
 *
 * @property \App\Model\Entity\Empleado $empleado
 * @property \App\Model\Entity\Caja $caja
 */
class CajaEmpleado extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'hire_date' => true,
        'empleado_id' => true,
        'caja_id' => true,
        'empleado' => true,
        'caja' => true
    ];
}
