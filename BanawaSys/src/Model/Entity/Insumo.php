<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Insumo Entity
 *
 * @property int $id
 * @property float $quantity
 * @property string $measure_unity
 * @property float $buy_amount
 * @property string $name
 * @property float $price
 * @property \Cake\I18n\FrozenDate $buy_date
 * @property string $provider
 * @property float $waste
 * @property float $avalible_items
 *
 * @property \App\Model\Entity\Recetum[] $receta
 */
class Insumo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'quantity' => true,
        'measure_unity' => true,
        'buy_amount' => true,
        'name' => true,
        'price' => true,
        'buy_date' => true,
        'provider' => true,
        'waste' => true,
        'avalible_items' => true,
        'receta' => true
    ];
}
