<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Descuento Entity
 *
 * @property int $id
 * @property string $type
 * @property float $price
 * @property int $quantity
 * @property int $avalible_amount
 *
 * @property \App\Model\Entity\Producto[] $producto
 * @property \App\Model\Entity\Venta[] $venta
 */
class Descuento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type' => true,
        'price' => true,
        'quantity' => true,
        'avalible_amount' => true,
        'producto' => true,
        'venta' => true
    ];
}
