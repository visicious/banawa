<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ventum Entity
 *
 * @property int $id
 * @property float $total_price
 * @property \Cake\I18n\FrozenTime $date
 * @property string $type
 * @property bool $is_nulled
 * @property bool $is_complain
 * @property string $complain_detail
 * @property string $null_detail
 * @property int $caja_id
 * @property int $descuento_id
 *
 * @property \App\Model\Entity\Caja $caja
 * @property \App\Model\Entity\Descuento $descuento
 * @property \App\Model\Entity\Producto[] $producto
 */
class Venta extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'total_price' => true,
        'date' => true,
        'type' => true,
        'is_nulled' => true,
        'is_complain' => true,
        'complain_detail' => true,
        'null_detail' => true,
        'caja_id' => true,
        'descuento_id' => true,
        'factura_RUC' => true,
        'factura_razon_social' => true,
        'order_name' => true,
        'pay_amount' => true,
        'pay_change' => true,
        'cod_documento' => true,
        'caja' => true,
        'descuento' => true,
        'producto' => true
    ];
}
