<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Caja Entity
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $user
 * @property string $password
 * @property string $original_password
 * @property string $location
 * @property int $workers_amount
 *
 * @property \App\Model\Entity\Ventum[] $venta
 * @property \App\Model\Entity\Empleado[] $empleado
 */
class Caja extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'type' => true,
        'user' => true,
        'password' => true,
        'original_password' => true,
        'location' => true,
        'workers_amount' => true,
        'venta' => true,
        'empleado' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
