<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * VentaProducto Entity
 *
 * @property int $id
 * @property int $amount
 * @property int $producto_id
 * @property int $venta_id
 *
 * @property \App\Model\Entity\Producto $producto
 * @property \App\Model\Entity\Ventum $venta
 */
class VentaProducto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'amount' => true,
        'producto_id' => true,
        'venta_id' => true,
        'producto' => true,
        'venta' => true
    ];
}
