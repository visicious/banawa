<?php
namespace App\View;

use Cake\View\View;

class HomeView extends AppView {

    public function initialize()
    {
        // Always enable the MyUtils Helper
        $this->loadHelper('MyUtils');
    }
}
