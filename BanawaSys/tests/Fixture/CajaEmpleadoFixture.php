<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CajaEmpleadoFixture
 *
 */
class CajaEmpleadoFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'caja_empleado';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'hire_date' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'empleado_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'caja_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_empleado_caja_id' => ['type' => 'index', 'columns' => ['empleado_id'], 'length' => []],
            'fk_caja_id' => ['type' => 'index', 'columns' => ['caja_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_caja_id' => ['type' => 'foreign', 'columns' => ['caja_id'], 'references' => ['caja', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_empleado_caja_id' => ['type' => 'foreign', 'columns' => ['empleado_id'], 'references' => ['empleado', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'hire_date' => '2018-03-11',
            'empleado_id' => 1,
            'caja_id' => 1
        ],
    ];
}
