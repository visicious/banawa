<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CajaEmpleadoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CajaEmpleadoTable Test Case
 */
class CajaEmpleadoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CajaEmpleadoTable
     */
    public $CajaEmpleado;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.caja_empleado',
        'app.empleado',
        'app.caja',
        'app.venta',
        'app.descuentos',
        'app.producto',
        'app.categoria',
        'app.receta',
        'app.insumo',
        'app.venta_producto'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CajaEmpleado') ? [] : ['className' => CajaEmpleadoTable::class];
        $this->CajaEmpleado = TableRegistry::get('CajaEmpleado', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CajaEmpleado);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
