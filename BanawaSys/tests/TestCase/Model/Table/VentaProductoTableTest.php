<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VentaProductoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VentaProductoTable Test Case
 */
class VentaProductoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VentaProductoTable
     */
    public $VentaProducto;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.venta_producto',
        'app.producto',
        'app.descuentos',
        'app.venta',
        'app.caja',
        'app.empleado',
        'app.caja_empleado',
        'app.categoria',
        'app.receta',
        'app.insumo'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('VentaProducto') ? [] : ['className' => VentaProductoTable::class];
        $this->VentaProducto = TableRegistry::get('VentaProducto', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VentaProducto);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
