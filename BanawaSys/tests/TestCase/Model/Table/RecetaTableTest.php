<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RecetaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RecetaTable Test Case
 */
class RecetaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RecetaTable
     */
    public $Receta;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.receta',
        'app.insumo',
        'app.producto',
        'app.descuentos',
        'app.venta',
        'app.caja',
        'app.empleado',
        'app.caja_empleado',
        'app.venta_producto',
        'app.categoria'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Receta') ? [] : ['className' => RecetaTable::class];
        $this->Receta = TableRegistry::get('Receta', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Receta);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
