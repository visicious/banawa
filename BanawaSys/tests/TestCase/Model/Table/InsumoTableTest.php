<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InsumoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InsumoTable Test Case
 */
class InsumoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InsumoTable
     */
    public $Insumo;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.insumo',
        'app.receta',
        'app.producto',
        'app.descuentos',
        'app.venta',
        'app.caja',
        'app.empleado',
        'app.caja_empleado',
        'app.venta_producto',
        'app.categoria'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Insumo') ? [] : ['className' => InsumoTable::class];
        $this->Insumo = TableRegistry::get('Insumo', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Insumo);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
