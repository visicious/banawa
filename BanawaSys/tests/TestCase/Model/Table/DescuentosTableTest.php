<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DescuentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DescuentosTable Test Case
 */
class DescuentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DescuentosTable
     */
    public $Descuentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.descuentos',
        'app.producto',
        'app.categoria',
        'app.receta',
        'app.insumo',
        'app.venta',
        'app.caja',
        'app.empleado',
        'app.caja_empleado',
        'app.venta_producto'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Descuentos') ? [] : ['className' => DescuentosTable::class];
        $this->Descuentos = TableRegistry::get('Descuentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Descuentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
