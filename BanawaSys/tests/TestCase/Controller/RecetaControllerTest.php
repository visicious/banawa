<?php
namespace App\Test\TestCase\Controller;

use App\Controller\RecetaController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\RecetaController Test Case
 */
class RecetaControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.receta',
        'app.insumo',
        'app.producto',
        'app.descuentos',
        'app.venta',
        'app.caja',
        'app.empleado',
        'app.caja_empleado',
        'app.venta_producto',
        'app.categoria'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
