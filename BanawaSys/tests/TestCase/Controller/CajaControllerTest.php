<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CajaController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CajaController Test Case
 */
class CajaControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.caja',
        'app.venta',
        'app.descuentos',
        'app.producto',
        'app.categoria',
        'app.receta',
        'app.insumo',
        'app.venta_producto',
        'app.empleado',
        'app.caja_empleado'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
